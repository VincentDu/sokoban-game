#!/bin/bash

javac -cp backend/maps/org.json.jar backend/game/*.java backend/maps/Map.java frontend/props/*.java
java -cp .:backend/maps/org.json.jar: frontend.props.View

rm backend/*/*.class
rm frontend/props/*.class