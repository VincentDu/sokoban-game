package frontend.props;

import java.util.HashMap;
import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Item {

    protected HashMap<String, Image> imagesMap;

    /* constructor of Item
     */
    public Item() {
        loadImages();
    }

    /* Loads images needed into a HashMap (compile from sokoban folder)
     */
    public void loadImages() {
        try {
            imagesMap = new HashMap<>();
            imagesMap.put("p", ImageIO.read(new File("frontend/props/images/character1.png"))); // character sheet in front
            imagesMap.put("p2", ImageIO.read(new File("frontend/props/images/character2.png"))); // character's back sheet
            imagesMap.put("Sokoban", ImageIO.read(new File("frontend/props/images/Sokoban.png"))); // Sokoban image
            imagesMap.put("Sokoban2", ImageIO.read(new File("frontend/props/images/Sokoban2.jpg"))); // Sokoban image
            imagesMap.put("w", ImageIO.read(new File("frontend/props/images/wall.png"))); // wall image
            imagesMap.put("b", ImageIO.read(new File("frontend/props/images/woodbox.png"))); // box image
            imagesMap.put("d", ImageIO.read(new File("frontend/props/images/environment-sheet0.png"))); // destination
            imagesMap.put("f", ImageIO.read(new File("frontend/props/images/greenBlock.png"))); // box and destination
            imagesMap.put("Sokoban3", ImageIO.read(new File("frontend/props/images/BackGround3.png"))); // box and destination
            imagesMap.put("r", ImageIO.read(new File("frontend/props/images/Gomme.jpg"))); // rubber
            imagesMap.put("t", ImageIO.read(new File("frontend/props/images/Trash.jpg"))); // trash
            imagesMap.put("save", ImageIO.read(new File("frontend/props/images/save.png"))); // trash
            imagesMap.put("back", ImageIO.read(new File("frontend/props/images/btnlevel-sheet1.png"))); //background 
            imagesMap.put("solo", ImageIO.read(new File("frontend/props/images/character_solo.png"))); //character solo

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}