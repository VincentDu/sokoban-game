package frontend.props;

import java.awt.*;
import javax.swing.*;

/**
 * This is the welcome JPanel which contains
 * the background image
 */
public class WelcomePanel extends JPanel {

    private Image bgImg;
    private JButton ghostButton;

    /**
     * constructor of WelcomePanel
     */
    public WelcomePanel(Image img) {
        bgImg = img;
        this.setPreferredSize(new Dimension(bgImg.getWidth(null), bgImg.getHeight(null)));
        ghostButton = new JButton(); //a invisible buttonwhoch cover our this
        this.setLayout(new BorderLayout());
        ghostButton.setOpaque(true);
        ghostButton.setContentAreaFilled(false);
        ghostButton.setBorderPainted(false);
        this.add(ghostButton);

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0, 0, this.getWidth(), this.getHeight(), null);
    }

    public Image getBgImg() {
        return bgImg;
    }

    public void setBgImg(Image bgImg) {
        this.bgImg = bgImg;
    }

    public JButton getGhostButton() {
        return ghostButton;
    }

    public void setGhostButton(JButton ghostButton) {
        this.ghostButton = ghostButton;
    }
}