package frontend.props;

import javax.swing.*;
import backend.game.Level;
import java.awt.*;
import java.util.HashMap;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Toolkit;

public class View extends JFrame {

    private Item item;
    private HashMap<String, Image> imagesMap; // contains all game's images
    private Image bg; // Backgroud of View
    private Image bg2; // Backgroud
    private Image bg3; // Backgroud
    private WelcomePanel welcomePanel; // welcome Panel
    private MenuPanel menuPanel; // main Panel containing the menu page
    private CardLayout cl = new CardLayout();
    private JPanel content = new JPanel();
    private BoardPanel boardPanel; // Main Panel containing visual game, Capucine's panel
    private CreatorPanel creatorPanel;
    private ListLevels listOfLevels;
    private String[] listPanel = { "welcomePanel", "menuPanel", "boardPanel", "CreatorPanel", "listLevels" };

    /**
     * constructor of view
     */
    public View() {
    	EventQueue.invokeLater(() -> {
    		item = new Item();
   			imagesMap = item.imagesMap;
    		// Create a window
    		this.setTitle("Sokoban");
    		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setSize(new Dimension(1920, 1080));
    		content.setLayout(cl);
    		this.add(content);

    		welcome_panel();
    		content.add(welcomePanel, listPanel[0]);

    		// Set the Menu Panel with all basic actions
    		menu_config();
    		content.add(menuPanel, listPanel[1]);

    		try {
    			creatorPanel = new CreatorPanel(bg3, this);
                content.add(creatorPanel, listPanel[3]);

                listOfLevels = new ListLevels(imagesMap.get("Sokoban2"), this);
                content.add(listOfLevels, listPanel[4]);

                // Need MapsStored to not be empty
                boardPanel = new BoardPanel(Level.getSortedFixedLevels().get(0).getBoard().getBoard(), this, "Fixed Map 01");
                boardPanel.setName("Fixed Map 01");
                content.add(boardPanel, listPanel[2]);
            } catch (Exception e) {
    			e.printStackTrace();
    		}

    		// Display the window
    		this.pack();
    		//Toolkit outil = getToolkit(); //avoid the full screen
    		//this.setSize(outil.getScreenSize()); ////avoid the full screen
    		this.setLocationRelativeTo(null);
    		this.setVisible(true);
    	});
    }

    /**
     * Configuration of welcome
     * When we launch the application
     */
    public void welcome_panel() {
        // Set Main Menu Panel background image
        bg = imagesMap.get("Sokoban");
        welcomePanel = new WelcomePanel(bg);

        // Play button will create a Game and remove old Panel
        welcomePanel.getGhostButton().addActionListener((Event) -> {
            cl.show(content, listPanel[1]);
        });
    }

    /**
     * Configuration of the Menu panel
     * When we click on the welcome panel
     */
    public void menu_config() {
        // Set Main Menu Panel background image
        bg2 = imagesMap.get("Sokoban2");
        menuPanel = new MenuPanel(bg2, this);

        // Play button will create a Game and remove old Panel
        menuPanel.getPlayButton().addActionListener((Event) -> {

            cl.show(content, listPanel[4]);

        });

        // Help button will create a Pop-Up
        menuPanel.getHelpButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(menuPanel,
                    "Bonjour et bienvenue dans notre jeu Sokoban !\n\n"
                	+"Le principe de ce jeu est d'amener la ou les caisses à un point d'arrivée precis.\n"
                    +"Vous ne pouvez que pousser les caisses et qu'une seule à la fois,\n"
                	+"bien sur vous risquez d'etre bloqué par les murs ou par d'autres caisses.\n"
                    +"A vous d'etre assez malin et de vous en sortir !\n\n"
                    +"Pour vous deplacer vous devez utiliser les fleches de votre clavier.\n"
                	+"Et il vous est aussi possible de créer vos propres niveaux !!! \n\n"
                    + "Sur ce, bon jeu et amusez vous bien !");
            }
        });

        // Create button will make the link between the menu and the level creator
        menuPanel.getCreateButton().addActionListener((Event) -> {

            cl.show(content, listPanel[3]);

        });

        bg3 = imagesMap.get("Sokoban3");

    }

    public void removeAllKeyListner() {
        for(int i = 0; i < this.getKeyListeners().length; i++) {
            this.removeKeyListener(this.getKeyListeners()[i]);
        }
    }

    public static void main(String[] args) {
        View v = new View();
    }

    public CardLayout getCl() {
        return cl;
    }

    public void setCl(CardLayout cl) {
        this.cl = cl;
    }

    public String[] getListPanel() {
        return listPanel;
    }

    public void setListPanel(String[] listPanel) {
        this.listPanel = listPanel;
    }

    public JPanel getContent() {
        return content;
    }

    public void setContent(JPanel content) {
        this.content = content;
    }

    public BoardPanel getBoardPanel() {
        return boardPanel;
    }

    public void setBoardPanel(BoardPanel boardPanel) {
        this.boardPanel = boardPanel;
    }

    public ListLevels getListOfLevels() {
        return listOfLevels;
    }

    public void setListOfLevels(ListLevels listOfLevels) {
        this.listOfLevels = listOfLevels;
    }

}