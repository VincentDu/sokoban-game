package frontend.props;

import java.awt.*;
import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This is the menu JPanel which contains
 * the background image
 */
public class MenuPanel extends JPanel {
    private View view;

    private Image bgImg;
    private JPanel menu; // Panel menu containing buttons (play, help, etc...)

    private JButton playButton; //play button
    private JButton helpButton; //help button
    private JButton createButton; //create new level
    private JButton exitButton;

    private AudioInputStream music; // Music of the game
    private Clip clip; // Allows us to stop and start of the music
    private int cpt; // Avoid multiple play music

    private JPanel usefull1; //usefull1, BorderLayout.NORTH
    private JPanel usefull2; //usefull2, BorderLayout.WEST
    private JPanel usefull3; //usefull3, BorderLayout.EAST
    private JPanel usefull4; //usefull4, BorderLayout.SOUTH
    private final Font fontEntered = new Font(Font.DIALOG, Font.ITALIC, 90); //mouse on the button
    private final Font fontExited = new Font(Font.DIALOG, Font.ITALIC, 65); //mousse exit the button
    private final Font fontEntered2 = new Font(Font.DIALOG, Font.ITALIC, 40); //mouse on the button
    private final Font fontExited2 = new Font(Font.DIALOG, Font.ITALIC, 30); //mousse exit the button

    /**
     * constructor of MenuPanel
     */
    public MenuPanel(Image img, View view) {
        this.view = view;
        bgImg = img;
        this.setPreferredSize(new Dimension(bgImg.getWidth(null), bgImg.getHeight(null)));
        playAndHelpButtons();
        exitButtonConfig();
        menu = new JPanel(new GridLayout(5, 1, 20, 20));
        menu.setBackground(Color.BLACK);
        menu.setPreferredSize(new Dimension(bgImg.getWidth(null)/4, bgImg.getHeight(null)/4));
        menu.setOpaque(false);
        menu.add(playButton);
        menu.add(createButton);
        theMusic();
        menu.add(helpButton);
        menu.add(exitButton);
        this.setLayout(new BorderLayout());
        uselessPanels();
        this.add(usefull1, BorderLayout.NORTH);
        this.add(usefull2, BorderLayout.WEST);
        this.add(usefull3, BorderLayout.EAST);
        this.add(usefull4, BorderLayout.SOUTH);
        this.add(menu, BorderLayout.CENTER);

    }

    /**
     * the configuration of exit Button
     */
    public void exitButtonConfig() {
        exitButton = new JButton("Exit");
        exitButton.setContentAreaFilled(false);
        exitButton.setBorderPainted(false);
        exitButton.setFocusPainted(false);
        exitButton.setFont(fontExited2);
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.dispose();
            }
        });

        exitButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                exitButton.setFont(fontEntered2);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) { //back to the last initial bg
                exitButton.setFont(fontExited2);
            }
        });
    }

    /**
     * Configuration of the play and help buttons
     */
    public void playAndHelpButtons() {
        playButton = new JButton("Play");
        playButton.setPreferredSize(new Dimension(bgImg.getWidth(null)/10, bgImg.getHeight(null)/9));
        playButton.setContentAreaFilled(false);
        playButton.setBorderPainted(false);
        playButton.setFocusPainted(false); //button border when you click on it
        playButton.setFont(fontExited);
        playButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                playButton.setFont(fontEntered);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) { //back to the last initial bg
                playButton.setFont(fontExited);
            }
        });
        helpButton = new JButton("Help");
        helpButton.setPreferredSize(new Dimension(bgImg.getWidth(null)/10, bgImg.getHeight(null)/9));
        helpButton.setContentAreaFilled(false);
        helpButton.setBorderPainted(false);
        helpButton.setFocusPainted(false);
        helpButton.setFont(fontExited);
        helpButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                helpButton.setFont(fontEntered);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) { //back to the last initial bg
                helpButton.setFont(fontExited);
            }
        });

        createButton = new JButton("Create My Level");
        createButton.setPreferredSize(new Dimension(bgImg.getWidth(null)/10, bgImg.getHeight(null)/9));
        createButton.setContentAreaFilled(false);
        createButton.setBorderPainted(false);
        createButton.setFocusPainted(false);
        createButton.setFont(fontExited);
        createButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                createButton.setFont(fontEntered);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) { //back to the last initial bg
                createButton.setFont(fontExited);
            }
        });
    }

    /**
     * Jpanels setup
     */
    public void uselessPanels() {
        usefull1 = new JPanel();
        usefull1.setPreferredSize(new Dimension(bgImg.getWidth(null)/5, bgImg.getHeight(null)/2));
        usefull2 = new JPanel();
        usefull2.setPreferredSize(new Dimension(bgImg.getWidth(null)/10, bgImg.getHeight(null)/10));
        usefull3 = new JPanel();
        usefull3.setPreferredSize(new Dimension(bgImg.getWidth(null)/2, bgImg.getHeight(null)/2));
        usefull4 = new JPanel();
        usefull4.setPreferredSize(new Dimension(bgImg.getWidth(null)/5, bgImg.getHeight(null)/10));
        usefull1.setOpaque(false);
        usefull2.setOpaque(false);
        usefull3.setOpaque(false);
        usefull4.setOpaque(false);
    }

    /**
     * Configuration of the music
     */
    public void theMusic() {
        // the music start when JFrame opened
        cpt = 1;
        try{
            music = AudioSystem.getAudioInputStream(new File("frontend/props/musics/MusicBy_LAKEYNoCopyRight.wav"));
            clip = AudioSystem.getClip();
            clip.open(music);
            // The music never end except if we click on "silence"
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            while (clip.isRunning()) {
                Thread.sleep(100);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        // Stop and replay the music
        JButton playMusic = new JButton("Mute/Unmute");
        playMusic.setContentAreaFilled(false);
        playMusic.setBorderPainted(false);
        playMusic.setFocusPainted(false);
        playMusic.setFont(fontExited);
        playMusic.addActionListener((Event) -> {
            if(cpt == 0) {
                try{
                    // Doest't work without music here
                    music = AudioSystem.getAudioInputStream(new File("frontend/props/musics/MusicBy_LAKEYNoCopyRight.wav"));
                    clip = AudioSystem.getClip();
                    clip.open(music);
                    clip.loop(Clip.LOOP_CONTINUOUSLY);
                    cpt ++;
                    while(clip.isRunning()) {
                        Thread.sleep(100);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }else {
            	//stop music
            	if (cpt == 1) {
                	cpt --;
                	clip.close();;
            	}
            }
        });

        playMusic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                playMusic.setFont(fontEntered);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) { //back to the last initial bg
                playMusic.setFont(fontExited);
            }
        });


        menu.add(playMusic);

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0, 0, this.getWidth(), this.getHeight(), null);
    }

    public Image getBgImg() {
        return bgImg;
    }

    public void setBgImg(Image bgImg) {
        this.bgImg = bgImg;
    }

    public JButton getPlayButton() {
        return playButton;
    }

    public void setPlayButton(JButton playButton) {
        this.playButton = playButton;
    }

    public JButton getHelpButton() {
        return helpButton;
    }

    public void setHelpButton(JButton helpButton) {
        this.helpButton = helpButton;
    }

    public JButton getCreateButton() {
        return createButton;
    }

    public void setCreateButton(JButton createButton) {
        this.createButton = createButton;
    }
}