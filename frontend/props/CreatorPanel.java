package frontend.props;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import backend.game.Board;
import backend.game.Level;

public class CreatorPanel extends JPanel {
    private View view;
    private int creatorPanelWidth;
    private int creatorPanelHeight;
    private Item item;
    private HashMap<String, Image> imagesMap; // contains all game's images
    private Image bgImg;
    private JPanel createPart;
    private JPanel submitPart;
    private JPanel map; // the map that we are creating
    private JPanel inMap; // the map that we are creating
    private JPanel allItemsPanel; // list of all the itemps

    private JPanel goBackPanel;
    private int goBackPanelWidth;
    private int goBackPanelHeight;
    private JButton backButton;

    private ArrayList<JButton> allItemsList;
    private int itemWidth; // Width of a item, change it in the constructor
    private int itemHeight; // Height of a item, change it in the constructor
    private int allItemsPanelWidth;
    private int allItemsPanelHeight;
    private Border thickBorder; // Border used for itemList

    private JButton[][] mapJButtons;
    private int mapPanelWidth;
    private int mapPanelHeight;
    private int mapJButtonWidth; // widtg of a item, change it in the constructor
    private int mapJButtonHeight; // Height of an button, change it in the constructor

    private JButton player;
    private String playerName = "p";
    private JButton wall;
    private String wallName = "w";
    private JButton box;
    private String boxName = "b";
    private JButton trash;
    private String trashName = "t";
    private JButton destination;
    private String destinationName = "d";
    private JButton save;
    private String saveName = "save";

    private String name; //the name of the map

    private int cptDestination = 0;
    private int cptBox = 0;
    private int cpt = 0; // only one item can have its border painted

    /**
     * @param img for the background Constructor of CreatorPanel
     * @throws Exception
     */
    CreatorPanel(Image img, View view) throws Exception {
        this.view = view;
        bgImg = img;
        creatorPanelWidth = 1920;
        creatorPanelHeight = 1080;
        item = new Item();
        imagesMap = item.imagesMap;

        createPart = new JPanel();
        createPart.setLayout(new BorderLayout());
        submitPart = new JPanel();
        submitPart.setLayout(new GridLayout(2, 1));

        goBackPanelWidth = 60;
        goBackPanelHeight = creatorPanelHeight;

        mapJButtons = new JButton[20][20];
        mapPanelWidth = 1660; //old size 1720
        mapPanelHeight = creatorPanelHeight;
        mapJButtonWidth = mapPanelWidth/20;
        mapJButtonHeight = mapPanelHeight/20;

        allItemsList = new ArrayList<>();
        thickBorder = new LineBorder(Color.WHITE, 12);
        allItemsPanelWidth = 200;
        allItemsPanelHeight = creatorPanelHeight;
        itemWidth = allItemsPanelWidth;
        itemHeight = allItemsPanelHeight/6;

        allItems_config();
        map_config();
        back_config();

        createPart.add(map, BorderLayout.CENTER);
        createPart.add(allItemsPanel, BorderLayout.EAST);
        createPart.add(goBackPanel, BorderLayout.WEST);
        this.setLayout(new BorderLayout());
        this.add(createPart);

        Level.loadLevels(); //available
    }

    /**
     * the configuration of the back button
     */
    public void back_config() {
        goBackPanel = new JPanel(new BorderLayout());
        goBackPanel.setPreferredSize(new Dimension(goBackPanelWidth, goBackPanelHeight));
        backButton = new JButton("<=");
        backButton.setPreferredSize(new Dimension(goBackPanelWidth, goBackPanelHeight));

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.getCl().show(view.getContent(), view.getListPanel()[1]);
            }
        });

        goBackPanel.add(backButton);
    }

    /**
     * all Items_config contains a list of items
     */
    public void allItems_config() {

        wall = new JButton();
        wall.setName(wallName);
        changeImageSize(wall, imagesMap.get("w"), itemWidth, itemHeight); // old size (allItemsPanel.getWidth(),
                                                                          // allItemsPanel.getHeight()/6);
        box = new JButton();
        box.setName(boxName);
        changeImageSize(box, imagesMap.get("b"), itemWidth, itemHeight);

        destination = new JButton();
        destination.setName(destinationName);
        changeImageSize(destination, imagesMap.get("d"), itemWidth, itemHeight);

        player = new JButton();
        player.setName(playerName);
        changeImageSize(player, imagesMap.get("solo"), itemWidth, itemHeight);

        trash = new JButton();
        trash.setName(trashName);
        changeImageSize(trash, imagesMap.get("t"), itemWidth, itemHeight);

        allItemsList.add(wall);
        allItemsList.add(box);
        allItemsList.add(destination);
        allItemsList.add(player);
        allItemsList.add(trash);
        allBorderFalse(allItemsList);
        selectOneItem(allItemsList);

        save = new JButton();
        save.setName(saveName);
        changeImageSize(save, imagesMap.get("save"), itemWidth, itemHeight);
        save.setBorderPainted(false);
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                char[][] myMatrix = jbuttonsMapToMatrix(mapJButtons);
                if(myMatrix == null)
                    return;
                Board myBoard = new Board(jbuttonsMapToMatrix(mapJButtons));
                if(!myBoard.verifyBoardCorrectlyClosed()) {
                    JOptionPane.showMessageDialog(null, "The map is not correctly closed");
                    return;
                }
                //JButton button = (JButton) e.getSource();
                name = JOptionPane.showInputDialog("Please enter the name of your map");
                if (name != null && name != "" && name.length() > 0 && isFilenameValid(name)) {
                    if (!Level.isNameTaken(name)) {
                        try {
                            boolean isSaved = Level.storeLevel(name, jbuttonsMapToMatrix(mapJButtons));
                            if(isSaved) { // Refresh List Levels to show new map created
                                view.getContent().remove(view.getListOfLevels());
                                view.setListOfLevels(new ListLevels(imagesMap.get("Sokoban2"), view));
                                view.getContent().add(view.getListOfLevels(), view.getListPanel()[4]);
                                view.getContent().revalidate();
                                view.getContent().repaint();
                                JOptionPane.showMessageDialog(null, "Level successfully saved !");
                            }
                            else
                                JOptionPane.showMessageDialog(null, "The map is not valid");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            JOptionPane.showMessageDialog(null, "Error");
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "This name is already taken !");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Wrong name !");
                }
            }
        });

        allItemsPanel = new JPanel(new GridLayout(6, 1));
        allItemsPanel.setPreferredSize(new Dimension(allItemsPanelWidth, allItemsPanelHeight)); // old size (bgImg.getWidth(null)/2,
                                                                  // bgImg.getHeight(null)/2))
        allItemsPanel.add(wall);
        allItemsPanel.add(box);
        allItemsPanel.add(destination);
        allItemsPanel.add(player);
        allItemsPanel.add(trash);
        allItemsPanel.add(save);
    }

    /**
     * resizing icon to fit on the JButoon in the Item List
     * it also change its border
     * @param button the JButton that we want to change icon
     * @param img    the image to fit on JButton
     * @param width  width of the JButton
     * @param height height of the JButton
     */
    public void changeImageSize(JButton button, Image img, int width, int height) {
        img = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        button.setIcon(new ImageIcon(img));
        button.setDisabledIcon(new ImageIcon(img));
        button.setBorder(thickBorder);
        // a.setBorderPainted(false);
        // a.setContentAreaFilled(false);
        // a.setVisible(false);
        // a.setOpaque(false);
    }

    /**
     * resizing icon to fit on the JButoon in the map because getSelectedItemIcon
     * and set its icon
     * @param button the JButton that we want to change icon
     * @param icon   the icon to fit on JButton
     * @param width  width of the JButton
     * @param height height of the JButton
     */
    public void changeIconSize(JButton button, Icon icon, int width, int height) {
        Image img = iconToImage(icon);
        img = (Image) img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        button.setIcon(new ImageIcon(img));
        button.setDisabledIcon(new ImageIcon(img));
    }

    /**
     * change Icon to Image
     * 
     * @param icon
     * @return Image
     */
    static Image iconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon) icon).getImage();
        } else {
            int w = icon.getIconWidth();
            int h = icon.getIconHeight();
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice gd = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gd.getDefaultConfiguration();
            BufferedImage image = gc.createCompatibleImage(w, h);
            Graphics2D g = image.createGraphics();
            icon.paintIcon(null, g, 0, 0);
            g.dispose();
            return image;
        }
    }

    /**
     * set all JButton's border to false in allItem
     * 
     * @param listOfButtons arraylist of our list of our Items
     */
    public void allBorderFalse(ArrayList<JButton> listOfButtons) {
        for (int i = 0; i < listOfButtons.size(); i++) {
            // if(listOfButtons.get(i) != excepButton)
            listOfButtons.get(i).setBorderPainted(false);
        }
    }

    /**
     * item selected border effect and trash function
     * @param listOfButtons arraylist of our list of our Items
     */
    public void selectOneItem(ArrayList<JButton> listOfButtons) {
        for (int i = 0; i < listOfButtons.size(); i++) {
            listOfButtons.get(i).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton button = (JButton) e.getSource();
                    if (!button.isBorderPainted()) {
                        allBorderFalse(listOfButtons);
                        button.setBorderPainted(true);
                        button.setBorder(thickBorder);
                        if (button.getIcon() == trash.getIcon()) {// trash effect
                            removeAllIcon(mapJButtons);
                            cptBox = 0;
                            cptDestination = 0;
                            cpt = 0;
                        }
                    } else
                        button.setBorderPainted(false);
                }
            });
        }
    }

    /**
     * get item's icon that is selected
     * @param listOfButtons arraylist of our list of our Items
     * @return its icon
     */
    public Icon getSelectedItemIcon(ArrayList<JButton> listOfButtons) {
        for (int i = 0; i < listOfButtons.size(); i++) {
            if (listOfButtons.get(i).isBorderPainted())
                return listOfButtons.get(i).getIcon(); // only on item can be selected
        }
        return null; // no item selected
    }

    /**
     * get the name of theitem that is selected
     * @param listOfButtons arraylist of our list of our Items
     * @return its icon
     */
    public String getSelectedItemName(ArrayList<JButton> listOfButtons) {
        for (int i = 0; i < listOfButtons.size(); i++) {
            if (listOfButtons.get(i).isBorderPainted())
                return listOfButtons.get(i).getName(); // only on item can be selected
        }
        return null; // no item selected
    }

    /**
     * the map that we are creating contains a board of JButtons
     */
    public void map_config() {
        map = new JPanel();
        map.setLayout(new BorderLayout());
        inMap = new JPanel();
        //inMap = new InMap(this);
        map.setPreferredSize(new Dimension(mapPanelWidth, mapPanelHeight)); // old size bgImg.getWidth(null)/2, bgImg.getHeight(null)/2
        // map.setBackground(Color.RED);
        inMap.setLayout(new GridLayout(20, 20));
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                mapJButtons[i][j] = new JButton(); // "i"+i+" j"+j
                mapJButtons[i][j].setContentAreaFilled(false);
                mapJButtons[i][j].setFocusPainted(false);
                mapJButtons[i][j].setSize(mapJButtonWidth, mapJButtonHeight); // old size : inMap.getHeight()/20,
                                                                              // inMap.getWidth()/20
                mapJButtons[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JButton button = (JButton) e.getSource();
                        if (button.getIcon() == null || button.getDisabledIcon() == null) { // get the selected item
                            if (getSelectedItemIcon(allItemsList) != null
                                    && getSelectedItemIcon(allItemsList) != trash.getIcon()) {
                                if (getSelectedItemIcon(allItemsList) == player.getIcon()) {
                                    if (cpt == 0) { // No player on the map
                                        button.setBackground(null);
                                        changeIconSize(button, getSelectedItemIcon(allItemsList), button.getWidth(),
                                                button.getHeight());
                                        ++cpt;
                                        button.setName(playerName);
                                        button.setOpaque(false);
                                    } else {
                                        removePlayer(mapJButtons);
                                        changeIconSize(button, getSelectedItemIcon(allItemsList), button.getWidth(),
                                                button.getHeight());
                                        button.setName(playerName);
                                        button.setOpaque(false);
                                    }
                                } else {
                                    changeIconSize(button, getSelectedItemIcon(allItemsList), button.getWidth(),
                                            button.getHeight());
                                    button.setName(getSelectedItemName(allItemsList));
                                    button.setBackground(null);
                                    if (button.getName().equals(destinationName)) // remove player item
                                        cptDestination ++;
                                    if (button.getName().equals(boxName)) // remove player item
                                        cptBox ++;
                                }
                            }
                        } else {
                            if (button.getName() != null && button.getName().equals(playerName)) // remove player item
                                cpt--;
                            if (button.getName() != null && button.getName().equals(destinationName)) // remove player item
                                cptDestination --;
                            if (button.getName() != null && button.getName().equals(boxName)) // remove player item
                                cptBox --;
                            button.setIcon(null); // removed its icon
                            button.setDisabledIcon(null);
                            button.setName(null);
                            button.setBackground(null);
                        }
                        drawRectangle();
                    }
                });
                inMap.add(mapJButtons[i][j]);
            }
        }
        map.add(inMap);
    }

    /**
     * remove all items in the JButton[][]
     * and set its icon and name to null
     * @param mapJButtons
     */
    public void removeAllIcon(JButton[][] mapJButtons) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                mapJButtons[i][j].setIcon(null); // removed its icon
                mapJButtons[i][j].setDisabledIcon(null);
                mapJButtons[i][j].setName(null);
                mapJButtons[i][j].setBackground(null);
                mapJButtons[i][j].setOpaque(false);
            }
        }
    }

    /**
     * remove player item in the JButton[][]
     * by setting its icon and name to null
     * @param mapJButtons
     */
    public void removePlayer(JButton[][] mapJButtons) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (mapJButtons[i][j].getIcon() != null && mapJButtons[i][j].getName() != null) {
                    if (mapJButtons[i][j].getName().equals(playerName)) {
                        mapJButtons[i][j].setIcon(null); // removed its icon
                        mapJButtons[i][j].setDisabledIcon(null);
                        mapJButtons[i][j].setName(null);
                        mapJButtons[i][j].setBackground(null);
                        return; // there s only one player
                    }
                }
            }
        }
    }

    /**
     * check if there's a player on the map
     * @param mapJButtons
     * @return boolean
     */
    public boolean haveAPlayer(JButton[][] mapJButtons) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (mapJButtons[i][j].getIcon() != null && mapJButtons[i][j].getName() != null) {
                    if (mapJButtons[i][j].getName().equals(playerName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * This will disable the given JButton
     * 
     * @param button JButton
     */
    public void disableButton(JButton button) {
        if (button != null) { // true == Fabriques
            button.setBorderPainted(false);
        }
    }

    /**
     * get the position of minimized mapJButtons
     * 
     * @param mapJButtons
     * @return int[] position = new int[4]; //x1, y1, x2, y2 = upper left and bottom
     *         right
     */
    public int[] minimize(JButton[][] mapJButtons) {
        ArrayList<Integer> xiPosition = new ArrayList<>();
        ArrayList<Integer> yjPosition = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (mapJButtons[i][j].getIcon() != null) {
                    //if (mapJButtons[i][j].getName() != null) {
                        xiPosition.add(i);
                        yjPosition.add(j);
                    //}
                }
            }
        }
        if (xiPosition.size() > 1 && yjPosition.size() > 1) {
            int[] position = new int[4]; // x1, y1, x2, y2 = upper left and bottom right
            position[0] = Collections.min(xiPosition);
            position[1] = Collections.min(yjPosition);
            position[2] = Collections.max(xiPosition);
            position[3] = Collections.max(yjPosition);
            return position;
        } else {
            return null;
        }
    }

    /**
     * 
     * setOpaque(flag) on all JButtons
     * @param mapJButtons
     * @param flag opaque true or false
     */
    public void allOpaque(JButton[][] mapJButtons, boolean flag) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (mapJButtons[i][j].getIcon() == null) {
                    mapJButtons[i][j].setOpaque(flag);
                }
            }
        }
    }

    /**
     * 
     * check if the map is empty
     * @param mapJButtons
     */
    public boolean isEmptyMap(JButton[][] mapJButtons) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (mapJButtons[i][j].getIcon() != null) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * show parties not taken in the map by drawing them in grey
     * @param mapJButtons
     */
    public void drawRectangle() {
        int[] minimized = minimize(mapJButtons);
        if(minimized != null) {
            allOpaque(mapJButtons, false);
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                    if (i < minimized[0] || j < minimized[1] || i > minimized[2] || j > minimized[3]) {
                        mapJButtons[i][j].setBackground(Color.GRAY.brighter());
                        mapJButtons[i][j].setOpaque(true);
                    }
                }
            }
        } else if (isEmptyMap(mapJButtons)) {
            allOpaque(mapJButtons, false);
        } else {
            allOpaque(mapJButtons, false);
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                    if (mapJButtons[i][j].getIcon() == null) {
                        mapJButtons[i][j].setBackground(Color.GRAY.brighter());
                        mapJButtons[i][j].setOpaque(true);
                    }
                }
            }
        }
        inMap.repaint();
    }

    /**
     * chech if the map has its destion(s) and 1 player
     * and nb of destinations == nb of boxes
     * @param mapJButtons
     */
    public boolean haveDestinationAndPlayer(JButton[][] mapJButtons) {
        boolean flag1 = false;
        boolean flag2 = false;
        boolean flag3 = false;
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (mapJButtons[i][j].getIcon() != null && mapJButtons[i][j].getName() != null) {
                    if (mapJButtons[i][j].getName().equals(playerName)) {
                        flag1 = true;
                    }
                    if (mapJButtons[i][j].getName().equals(destinationName)) {
                        flag2 = true;
                    }
                    if (mapJButtons[i][j].getName().equals(boxName)) {
                        flag3 = true;
                    }
                }
            }
        }
        if(! haveAPlayer(mapJButtons)) {
            JOptionPane.showMessageDialog(this,
                    "We need a player");
            return false;
        }
        if((cptDestination == 0 || cptBox == 0)) {
            JOptionPane.showMessageDialog(this,
                    "We need a destination and a boxe");
            return false;
        }
        if((cptDestination != cptBox)) {
            JOptionPane.showMessageDialog(this,
                    "We need the same number of destinations and boxes");
            return false;
        }
        return (flag1 && flag2 && flag3 && (cptDestination == cptBox)) ;
    }

    /**
     * get the first item in the line
     * @param mapJButtons
     * @param minimized map
     * @param i line
     * @return the columm of the first item or -1 if there is none
     */
    public int firstInLine(JButton[][] mapJButtons, int[] minimized, int i) {
        if(minimized != null) {
            for (int j = minimized[1]; j < minimized[3]+1; j++){
                if (mapJButtons[i][j].getIcon() != null) {
                    return j;
                }
            }
        }
        return -1;
    }

    /**
     * get the last item in the line
     * @param mapJButtons
     * @param minimized map
     * @param i line
     * @return the columm of the last item or -1 if there is none
     */
    public int lastInLine(JButton[][] mapJButtons, int[] minimized, int i) {
        int tmp = -1;
        if(minimized != null) {
            for (int j = minimized[1]; j < minimized[3]+1; j++){
                if (mapJButtons[i][j].getIcon() != null) {
                    tmp = j;
                }
            }
        }
        return tmp;
    }

    /**
     * convert maJButtons to a matrix
     * @param mapJButtons
     */
    public char[][] jbuttonsMapToMatrix(JButton[][] mapJButtons) {
        int[] minimized = minimize(mapJButtons);
        if(minimized != null && haveDestinationAndPlayer(mapJButtons)) {
            char[][] matrix = new char[minimized[2] - minimized[0]+1][minimized[3] - minimized[1]+1];
            int index1 = 0;
            int index2 = 0;
            for (int i = minimized[0]; i < minimized[2]+1; i++) {
                index2 = 0;
                for (int j = minimized[1]; j < minimized[3]+1; j++) {
                        if(mapJButtons[i][j].getIcon() != null)
                            matrix[index1][index2] = mapJButtons[i][j].getName().charAt(0);
                        else if(firstInLine(mapJButtons, minimized, i) != -1 && lastInLine(mapJButtons, minimized, i) != -1) {
                            if(j < firstInLine(mapJButtons, minimized, i) || j > lastInLine(mapJButtons, minimized, i))
                                matrix[index1][index2] = 'v';
                            else
                                matrix[index1][index2] = 'n';
                        }
                        else
                            matrix[index1][index2] = 'n';
                        index2 ++;
                }
                index1 ++;
            }
            return matrix;
        }
        return null;
    }

    /**
     * check if the name can be used to save
     * @param file
     */
    public static boolean isFilenameValid(String file) {
        File f = new File(file);
        try {
           f.getCanonicalPath();
           return true;
        }
        catch (IOException e) {
           return false;
        }
      }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0, 0, this.getWidth(), this.getHeight(), null);
    }

    public Image getBgImg() {
        return bgImg;
    }

    public void setBgImg(Image bgImg) {
        this.bgImg = bgImg;
    }

}