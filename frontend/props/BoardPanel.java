package frontend.props;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import backend.game.Board;
import backend.game.GameTerminal;
import backend.game.Level;

import java.awt.*;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

/**
 * This is the panel who create the Board and update it
 */

public class BoardPanel extends JPanel implements KeyListener{
    private int play = 0;
    private char[][] board;
    private char[][] savedBoard;
    private GameTerminal gameTerminal;
    private View view;
    private int boardPanelWidth = 1920;
    private int boardPanelHeight = 1080;
    private JLabel[][] labs;
    private JLabel scores;

    private JPanel contains;

    private JPanel goBackPanel;
    private int goBackPanelWidth;
    private int goBackPanelHeight;
    private JButton backButton;

    private JPanel jPan;
    private int jPanWidth;
    private int jPanHeight;

    private int buttonslWidth;
    private int buttonsHeight;

    private Item item;
    private HashMap<String, Image> imagesMap; // contains all board's images
    private Image wall;
    private Image box;
    private Image player;
    private Image destination;
    private Image empty;
    private Image overlap;
    private Image fin;
    private ImageIcon icW;;
    private ImageIcon icB;
    private ImageIcon icP;
    private ImageIcon icD;
    private ImageIcon icV;
    private ImageIcon icO;
    private ImageIcon icF;
    private ImageIcon icUP;
    private ImageIcon icLeft;
    private ImageIcon icRight;

    /**
     * Constructor of BoardPanel
     * extends JPanel, it contains the
     * given board as a display when created
     * @param board char[][]
     * @param View JPanel
     */
    public BoardPanel(char[][] board, View view, String name){
        this.setName(name);
        this.board = board;
        this.savedBoard = new char[board.length][board[0].length];
        for(int i=0; i<board.length; i++)
            savedBoard[i] = board[i].clone();
        this.view = view;
        addKeyListener(this);
        setFocusable(true);
        //this.requestFocus();
        gameTerminal = new GameTerminal(new Board(board));
        item = new Item();
        imagesMap = item.imagesMap;

        this.setBackground(Color.cyan);
        this.setOpaque(true);

        goBackPanelWidth = 60;
        goBackPanelHeight = boardPanelHeight;
        back_config();

        jPan = new JPanel();
        jPanWidth = 1860;
        jPanHeight = boardPanelHeight;
        myImages();
        jPan.setPreferredSize(new Dimension(jPanWidth, jPanHeight));
        jPan.setLayout(new GridLayout(board.length, board[0].length));
        jPan.setBorder(BorderFactory.createLineBorder(new Color(139,69,19), 10));
        jPan.setOpaque(true);
        this.display(board);

        // JLabel to display the current number of moves and record
        scores = new JLabel();
        String currentScore = String.valueOf(gameTerminal.getSteps());
        String scoreRecord = String.valueOf(Level.getSokobanLevels().get(this.getName()).getRecord());
        scores.setText("<html>Moves : " + currentScore + "<br>Record : " + scoreRecord + "</html>");

        // Set score label position and color, font, etc...
        scores.setForeground(new Color(239, 47, 47));
        scores.setOpaque(false);
        scores.setLocation(80, 5);
        scores.setFont(new Font("Serif", Font.BOLD, 40));
        scores.setSize(300, 100);
        this.setLayout(null);
        this.add(scores);

        contains = new JPanel();
        contains.setLayout(new BorderLayout());
        contains.add(jPan, BorderLayout.EAST);
        contains.add(goBackPanel, BorderLayout.WEST);
        this.setLayout(new BorderLayout());
        this.add(contains);
}

    /**
     * the configuration of the back button
     */
    public void back_config() {
        goBackPanel = new JPanel(new BorderLayout());
        goBackPanel.setPreferredSize(new Dimension(goBackPanelWidth, goBackPanelHeight));
        backButton = new JButton("<=");
        backButton.setPreferredSize(new Dimension(goBackPanelWidth, goBackPanelHeight));

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i=0; i<board.length; i++)
                    board[i] = savedBoard[i].clone();
                displayChanges(savedBoard, "");
                gameTerminal = new GameTerminal(new Board(board));
                repaint();
                view.getCl().show(view.getContent(), view.getListPanel()[4]);
            }
        });

        goBackPanel.add(backButton);
    }

    public static BufferedImage toBufferedImage(Image img)
    {
    if (img instanceof BufferedImage)
    {
        return (BufferedImage) img;
    }

    // Create a buffered image with transparency
    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

    // Draw the image on to the buffered image
    Graphics2D bGr = bimage.createGraphics();
    bGr.drawImage(img, 0, 0, null);
    bGr.dispose();

    // Return the buffered image
    return bimage;
    }

    /**
     * the configuration of images
     */
    public void myImages() {
        buttonslWidth = jPanWidth/this.board[0].length;
        buttonsHeight = jPanHeight/this.board.length;

        wall = imagesMap.get("w").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        box = imagesMap.get("b").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        player = imagesMap.get("solo").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        destination = imagesMap.get("d").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        empty = imagesMap.get("back").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        overlap = imagesMap.get("solo").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        fin = imagesMap.get("f").getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        BufferedImage playerProfils = toBufferedImage(imagesMap.get("p"));
        BufferedImage playerProfils2 = toBufferedImage(imagesMap.get("p2"));
        Image pUp = (Image)playerProfils2.getSubimage(0, 0, 65, 65).getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        Image pLeft = (Image)playerProfils.getSubimage(0, 65, 65, 65).getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);
        Image pRight = (Image )playerProfils.getSubimage(0, 130, 65, 65).getScaledInstance(buttonslWidth, buttonsHeight, Image.SCALE_SMOOTH);

        icW = new ImageIcon(wall);
        icB = new ImageIcon(box);
        icP = new ImageIcon(player);
        icD = new ImageIcon(destination);
        icV = new ImageIcon(empty);
        icO = new ImageIcon(overlap);
        icF = new ImageIcon(fin);
        icUP = new ImageIcon(pUp);
        icLeft = new ImageIcon(pLeft);
        icRight = new ImageIcon(pRight);


    }


    /**
     * adds JLabel elements
     * corresponding to the given board's items,
     * The size of the objects can be modified through
     * the variable int=size (80 as example set)
     * @param b char[][]
     */
    public void display (char[][] b) {
        labs = new JLabel[b.length][b[0].length];
        Dimension dim = new Dimension(buttonslWidth, buttonsHeight);
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                JLabel lab = new JLabel();
                labs[i][j] = lab;
                lab.setPreferredSize(dim);
                lab.setOpaque(true);
                lab.setBackground(Color.DARK_GRAY);

                switch (b[i][j]) {

                case 'w':
                    lab.setIcon(icW);
                    jPan.add(lab);
                    break;

                case 'b':
                    lab.setIcon(icB);
                    jPan.add(lab);
                    break;

                case 'p':
                    lab.setIcon(icP);
                    jPan.add(lab);
                    break;

                case 'd':
                    lab.setIcon(icD);
                    jPan.add(lab);
                    break;

                case 'n':
                    jPan.add(lab);
                    break;

                case 'o':
                    lab.setIcon(icO);
                    jPan.add(lab);
                    break;

                case 'f':
                    lab.setIcon(icF);
                    jPan.add(lab);
                    break;

                case 'v':
                    lab.setIcon(icV);
                    jPan.add(lab);
                    break;

                default:
                    jPan.add(lab);
                    break;

                }
            }
        }
    }

    /**
     * edit JLabels with the right Icon
     * @param b char[][]
     */
    public void displayChanges (char[][] b, String move) {
        String currentScore = String.valueOf(gameTerminal.getSteps()); // Refresh Score
        String scoreRecord = String.valueOf(Level.getSokobanLevels().get(this.getName()).getRecord());
        scores.setText("<html>Moves : " + currentScore + "<br>Record : " + scoreRecord + "</html>");

        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                switch (b[i][j]) {

                case 'w':
                    labs[i][j].setIcon(icW);
                    break;

                case 'b':
                    labs[i][j].setIcon(icB);
                    break;

                case 'p':
                    if(move.equals("up"))
                        labs[i][j].setIcon(icUP);
                    else if(move.equals("left"))
                        labs[i][j].setIcon(icLeft);
                    else if(move.equals("right"))
                        labs[i][j].setIcon(icRight);
                    else
                        labs[i][j].setIcon(icP);
                    break;

                case 'd':
                    labs[i][j].setIcon(icD);
                    break;

                case 'n':
                    labs[i][j].setIcon(null);
                    break;

                case 'o':
                    if(move.equals("up"))
                        labs[i][j].setIcon(icUP);
                    else if(move.equals("left"))
                        labs[i][j].setIcon(icLeft);
                    else if(move.equals("right"))
                        labs[i][j].setIcon(icRight);
                    else
                        labs[i][j].setIcon(icP);;
                    break;

                case 'f':
                    labs[i][j].setIcon(icF);
                    break;

                case 'v':
                    labs[i][j].setIcon(icV);
                    break;

                default:
                    break;

                }
            }
        }
        jPan.repaint();
        this.repaint();
    }

    //4 = left,    2 = down ,   6 = right,  8 up
    @Override
    public void keyPressed(KeyEvent arg0) {

        int moved;

        if(arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
            moved = gameTerminal.moveState(6);
            gameTerminal.move(6, moved);
            board = gameTerminal.getBoard().getBoard();
            displayChanges(board, "right");
        } else if (arg0.getKeyCode() == KeyEvent.VK_LEFT){
            moved = gameTerminal.moveState(4);
            gameTerminal.move(4, moved);
            board = gameTerminal.getBoard().getBoard();
            displayChanges(board, "left");
        }
        else if (arg0.getKeyCode() == KeyEvent.VK_UP){
            moved = gameTerminal.moveState(8);
            gameTerminal.move(8, moved);
            board = gameTerminal.getBoard().getBoard();
            displayChanges(board, "up");
        }
        else if (arg0.getKeyCode() == KeyEvent.VK_DOWN){
            moved = gameTerminal.moveState(2);
            gameTerminal.move(2, moved);
            board = gameTerminal.getBoard().getBoard();
            displayChanges(board, "down");
        } else if (arg0.getKeyCode() == KeyEvent.VK_R) {
            for (int i = 0; i < board.length; i++)
                board[i] = savedBoard[i].clone();
            gameTerminal = new GameTerminal(new Board(board));
            displayChanges(savedBoard, "");
        }

        if(gameTerminal.verifyGameEnd()) {
            JOptionPane.showMessageDialog(null, "Congratulation, you win !");
            play = 0;
            view.getListOfLevels().repaint();
            this.repaint();

            try { // Set Level to cleared and refresh view
                Level.setLevelCleared(this.getName(), true, this.gameTerminal.getSteps());
                view.getContent().remove(view.getListOfLevels());
                view.setListOfLevels(new ListLevels(imagesMap.get("Sokoban2"), view));
                view.getContent().add(view.getListOfLevels(), view.getListPanel()[4]);
                view.getContent().revalidate();
                view.getContent().repaint();
            } catch (Exception e) {
                e.printStackTrace();
            }
            view.getCl().show(view.getContent(), view.getListPanel()[4]);
        }

    }

    @Override
    public void keyReleased(KeyEvent arg0) {

    }

    @Override
    public void keyTyped(KeyEvent arg0) {

    }

    public JPanel getjPan() {
        return jPan;
    }

    public void setjPan(JPanel jPan) {
        this.jPan = jPan;
    }

}
