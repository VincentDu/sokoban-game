package frontend.props;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import backend.game.Level;
import backend.game.Board;

/**
 * This is the panel which display the list of levels and update it
 */

public class ListLevels extends JPanel {

    private Image bgImg;
    private View view;
    private int listLevelsWidth = 1920;
    private int listLevelsHeight = 1080;

    private JPanel contains;

    private JPanel goBackPanel;
    private int goBackPanelWidth;
    private int goBackPanelHeight;
    private JButton backButton;

    private JPanel listPanel;
    private int listPanelWidth;
    private int listPanelHeight;

    private ArrayList<Level> fixedLevelsList;
    private ArrayList<Level> originalLevelsList;

    public ListLevels(Image img, View view) throws Exception {

        try {
            initLists();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.view = view;
        bgImg = img;
        contains = new JPanel();
        contains.setLayout(new BorderLayout());

        goBackPanelWidth = 60;
        goBackPanelHeight = listLevelsHeight;
        back_config();

        listPanelWidth = 1860;
        listPanelHeight = listLevelsHeight;
        listPanelConfig();

        contains.add(goBackPanel, BorderLayout.WEST);
        contains.add(listPanel, BorderLayout.EAST);
        this.setLayout(new BorderLayout());
        this.add(contains);
    }

    /**
     * initialise arraylists
     */
    public void initLists() throws Exception{
        try {
            Level.loadLevels();
            fixedLevelsList = Level.getSortedFixedLevels();
            originalLevelsList = Level.getSortedOriginalLevels();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * the configuration of list
     */
    public void listPanelConfig() {

        //initialise the main panel
        listPanel = new JPanel();
        listPanel.setPreferredSize(new Dimension(listPanelWidth, listPanelHeight));
        listPanel.setBackground(Color.black);
        listPanel.setLayout(new BorderLayout());

        //initialise the CardLayout of originals or fixed maps
        CardLayout cl = new CardLayout();
        String[] listContent = { "Fixed", "Original" };

        //initialise the top buttons to chose witch maps we want to see
        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 2));
        buttons.setPreferredSize(new Dimension(1920, 50));
        JButton buttonFixed = new JButton("Normal Levels");
        JButton buttonOriginal = new JButton("My Levels");

        buttonFixed.setBorderPainted(false);
        buttonFixed.setFocusPainted(false); // button border when you click on it

        buttonOriginal.setBorderPainted(false);
        buttonOriginal.setFocusPainted(false); // button border when you click on it

        buttonFixed.setBackground(new Color(140, 90, 90));
        buttonOriginal.setBackground(new Color(80, 15, 15));
        Font fontButton = new Font(Font.DIALOG, Font.BOLD, 20);
        buttonFixed.setFont(fontButton);
        buttonOriginal.setFont(fontButton);
        buttonFixed.setForeground(new Color(255, 229, 204));
        buttonOriginal.setForeground(new Color(255, 229, 204));
        JPanel level = new JPanel();



        //add actions to the buttons
        buttonOriginal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                cl.show(level, listContent[1]);
            }
        });

        buttonFixed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                cl.show(level, listContent[0]);
            }
        });

        // initialise the panels where the buttons of levels will be
        JPanel listFixed = new JPanel();
        listFixed.setBackground(new Color(140, 90, 90));
        listFixed.setLayout(new GridLayout(5, 2, 50, 50));
        JPanel listOriginal = new JPanel();
        listOriginal.setBackground(new Color(80, 15, 15));
        listOriginal.setLayout(new GridLayout((originalLevelsList.size()/2)+2, 2, 50, 50));
        JScrollPane listOriginalContains = new JScrollPane(listOriginal);
        listOriginalContains.setBorder(null);

        // creation of the buttons and what they do (on listFixed panel)
        if(fixedLevelsList != null) {
            for(int i=0; i<fixedLevelsList.size(); i++) {
                JPanel jpan = new JPanel();
                jpan.setLayout(null);
                jpan.setBackground(new Color(140, 90, 90));

                String s = fixedLevelsList.get(i).getName();
                JButton button = new JButton(s);
                button.setName(String.valueOf(i));
                button.setBounds(350, 20, 150, 150);

                if(fixedLevelsList.get(i).getCleared()) {
                    button.setBorder(BorderFactory.createLineBorder(new Color(255, 204, 153), 8));
                    button.setBackground(Color.LIGHT_GRAY);
                } else {
                    button.setBackground(Color.LIGHT_GRAY);
                }

                char[][] board = fixedLevelsList.get(i).getBoard().getBoard();
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        view.getContent().remove(view.getBoardPanel());
                        view.setBoardPanel(new BoardPanel(board, view, s));
                        view.getBoardPanel().setName(s);
                        requestFocus();
                        view.getContent().add(view.getBoardPanel(), view.getListPanel()[2]);
                        view.getContent().revalidate();
                        view.getContent().repaint();
                        view.getCl().show(view.getContent(), view.getListPanel()[2]);
                    }
                });
                jpan.add(button);
                listFixed.add(jpan);
            }
        } else {
            System.out.println("il n'y a pas de map");
        }

    // creation of the buttons and what they do (on listFixed panel)
    if(originalLevelsList != null) {
        listOriginal.setPreferredSize(new Dimension(listOriginal.getWidth(), 1080+(1080 *(originalLevelsList.size()/5))));
        for(int i=0; i<originalLevelsList.size(); i++) {
                JPanel jpan = new JPanel();
                jpan.setLayout(null);
                jpan.setBackground(new Color(80, 15, 15));
                String s = originalLevelsList.get(i).getName();
                JButton button = new JButton(s);
                button.setName(String.valueOf(i));
                button.setBounds(350, 20, 150, 150);
                button.setVisible(true);

                if(originalLevelsList.get(i).getCleared()) {
                    button.setBorder(BorderFactory.createLineBorder(new Color(255, 204, 153), 8));
                    button.setBackground(Color.LIGHT_GRAY);
                }else {
                    button.setBackground(Color.LIGHT_GRAY);
                }
                char[][] board = originalLevelsList.get(i).getBoard().getBoard();
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        view.getContent().remove(view.getBoardPanel());
                        view.setBoardPanel(new BoardPanel(board, view, s));
                        view.getBoardPanel().setName(s);
                        requestFocus();
                        view.getContent().add(view.getBoardPanel(), view.getListPanel()[2]);
                        view.getContent().revalidate();
                        view.getContent().repaint();
                        view.getCl().show(view.getContent(), view.getListPanel()[2]);
                    }
                });

                jpan.add(button);
                listOriginal.add(jpan);
            }
        } else {
            System.out.println("il n'y a pas de map");
        }

        // add the panel in the CardLayout
        level.setLayout(cl);
        level.add(listFixed, listContent[0]);
        level.add(listOriginalContains, listContent[1]);

        //add buttons on top and levels in the middle
        buttons.add(buttonFixed);
        buttons.add(buttonOriginal);
        listPanel.add(level, BorderLayout.CENTER);
        listPanel.add(buttons, BorderLayout.NORTH);

    }

    /**
     * the configuration of the back button
     */
    public void back_config() {
        goBackPanel = new JPanel(new BorderLayout());
        goBackPanel.setPreferredSize(new Dimension(goBackPanelWidth, goBackPanelHeight));
        backButton = new JButton("<=");
        backButton.setPreferredSize(new Dimension(goBackPanelWidth, goBackPanelHeight));

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.getCl().show(view.getContent(), view.getListPanel()[1]);
            }
        });

        goBackPanel.add(backButton);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bgImg, 0, 0, this.getWidth(), this.getHeight(), null);
    }

}
