package backend.game;

import java.util.Scanner;

/**
 * This class is basically the game launched but in the terminal only. It
 * modifies the board according to functions called (move(), moveState(),
 * etc...)
 *
 * There are also some functions which launches a real time game in the terminal
 * for test purposes only (Used in Launch.java)
 */
public class GameTerminal {
    private Board board; // The game's board
    private int stepsCount; // Number of movements done
    static Scanner myObj;

    /**
     * Constructor Game
     *
     * @param board
     */
    public GameTerminal(Board board) {
        this.board = board;
        this.stepsCount = 0;
    }

    /**
     * Launches the game in the terminal TEST PURPOSES ONLY
     */
    public void run() {
        myObj = new Scanner(System.in); // Create a Scanner object
        System.out.println("Welcome to sokoban Terminal version");
        while (true) {
            this.Display();
            int tmp1 = this.Direction();
            int tmp2 = this.moveState(tmp1);
            if (verifyLegalMove(tmp2)) {
                this.move(tmp1, tmp2);
            } else {
                continue;
            }
            if (this.verifyGameEnd()) {
                break;
            }
        }
        myObj.close();
        this.Display();
        System.out.println("Game Won");

    }

    /**
     * Displays the board in the terminal TEST PURPOSES ONLY
     */
    public void Display() {
        for (int i = 0; i < this.board.getBoard().length; i++) {
            for (int j = 0; j < this.board.getBoard()[i].length; j++) {
                System.out.print(this.board.getBoard()[i][j]);
            }
            System.out.println();
        }

    }

    /**
     * Asks for the direction (movement) TEST PURPOSES ONLY
     *
     * @return int (2,4,6,8) = (down, left, right, up)
     */
    public int Direction() {

        System.out.println("Enter move");

        String rawIn = myObj.nextLine(); // Read user input
        char move = rawIn.charAt(0);
        int ret = 0;
        switch (move) {
            case 'z':
                ret = 8;
                break;
            case 's':
                ret = 2;
                break;
            case 'd':
                ret = 6;
                break;
            case 'q':
                ret = 4;
                break;
        }

        if (ret == 0) {
            return -1;
        }
        return ret;
        // return the value of the direction of the move for testing purposes
    }

    /**
     * A function that takes the direction of the input , tests for the state of the
     * move and returns a number: 0 means illegal move, 1 means legal move but no
     * box pushing involved , 2 means box pushing involved
     *
     * @param in int
     * @return int [0, 1, 2] = [Illegal, move, move box]
     */
    public int moveState(int in) {
        int x = this.board.getPlayerPos()[0];
        int y = this.board.getPlayerPos()[1];

        // A series of if tests that take into account the size of the board
        // x = 0 , y = 2 for test
        int ret = 0;
        if (in == 8) {
            if (y == 0)
                return 0;

            if (this.board.getBoard()[y - 1][x] == 'n' || this.board.getBoard()[y - 1][x] == 'd')
                ret = 1;

            if (this.board.getBoard()[y - 1][x] == 'b' || this.board.getBoard()[y - 1][x] == 'f') {
                if (y == 1)
                    return 0;
                if (this.board.getBoard()[y - 2][x] == 'b' || this.board.getBoard()[y - 2][x] == 'f'
                        || this.board.getBoard()[y - 2][x] == 'w') {
                    ret = 0;

                } else if (this.board.getBoard()[y - 2][x] == 'n' || this.board.getBoard()[y - 2][x] == 'd')
                    ret = 2;
            }
        } else if (in == 2) {
            if (y == this.board.getBoard().length - 1)
                return 0;

            if (this.board.getBoard()[y + 1][x] == 'n' || this.board.getBoard()[y + 1][x] == 'd')
                ret = 1;

            if (this.board.getBoard()[y + 1][x] == 'b' || this.board.getBoard()[y + 1][x] == 'f') {
                if (y == this.board.getBoard().length - 2)
                    return 0;

                if (this.board.getBoard()[y + 2][x] == 'b' || this.board.getBoard()[y + 2][x] == 'f'
                        || this.board.getBoard()[y + 2][x] == 'w') {
                    ret = 0;

                } else if (this.board.getBoard()[y + 2][x] == 'n' || this.board.getBoard()[y + 2][x] == 'd')
                    ret = 2;
            }
        }

        else if (in == 4) {
            if (x == 0)
                return 0;

            if (this.board.getBoard()[y][x - 1] == 'n' || this.board.getBoard()[y][x - 1] == 'd')
                ret = 1;

            if (this.board.getBoard()[y][x - 1] == 'b' || this.board.getBoard()[y][x - 1] == 'f') {
                if (x == 1)
                    return 0;

                if (this.board.getBoard()[y][x - 2] == 'b' || this.board.getBoard()[y][x - 2] == 'f'
                        || this.board.getBoard()[y][x - 2] == 'w') {
                    ret = 0;

                } else if (this.board.getBoard()[y][x - 2] == 'n' || this.board.getBoard()[y][x - 2] == 'd')
                    ret = 2;
            }
        }

        else if (in == 6) {
            if (x == this.board.getBoard()[y].length - 1)
                return 0;

            if (this.board.getBoard()[y][x + 1] == 'n' || this.board.getBoard()[y][x + 1] == 'd')
                ret = 1;

            if (this.board.getBoard()[y][x + 1] == 'b' || this.board.getBoard()[y][x + 1] == 'f') {
                if (x == this.board.getBoard()[y].length - 2)
                    return 0;

                if (this.board.getBoard()[y][x + 2] == 'b' || this.board.getBoard()[y][x + 2] == 'f'
                        || this.board.getBoard()[y][x + 2] == 'w') {
                    ret = 0;

                } else if (this.board.getBoard()[y][x + 2] == 'n' || this.board.getBoard()[y][x + 2] == 'd')
                    ret = 2;
            }
        }

        return ret;

    }

    /**
     * Verifies if the move is legal
     *
     * @param n int (movement 2,4,6,8)
     * @see int moveState(int in)
     * @return boolean
     */
    public boolean verifyLegalMove(int n) {
        if (n == 1 || n == 2) {
            return true;
        }
        return false;
    }

    /**
     * Moves the player
     *
     * @param n the move (2,4,6,8)
     * @see int moveState(int in)
     * @param mS call moveState(n)
     */
    public void move(int n, int mS) {
        if (!verifyLegalMove(mS))
            return;

        int x = this.board.getPlayerPos()[0];
        int y = this.board.getPlayerPos()[1];
        if (mS == 1) {
            if (n == 2) {

                if (this.board.getBoard()[y + 1][x] == 'd') {
                    this.board.getBoard()[y + 1][x] = 'o';
                } else {
                    this.board.getBoard()[y + 1][x] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x, y + 1 };
                this.board.setPlayerPos(tmp);
            }

            else if (n == 8) {

                if (this.board.getBoard()[y - 1][x] == 'd') {
                    this.board.getBoard()[y - 1][x] = 'o';
                } else {
                    this.board.getBoard()[y - 1][x] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x, y - 1 };
                this.board.setPlayerPos(tmp);
            }

            else if (n == 4) {

                if (this.board.getBoard()[y][x - 1] == 'd') {
                    this.board.getBoard()[y][x - 1] = 'o';
                } else {
                    this.board.getBoard()[y][x - 1] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x - 1, y };
                this.board.setPlayerPos(tmp);
            }

            else if (n == 6) {

                if (this.board.getBoard()[y][x + 1] == 'd') {
                    this.board.getBoard()[y][x + 1] = 'o';
                } else {
                    this.board.getBoard()[y][x + 1] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x + 1, y };
                this.board.setPlayerPos(tmp);
            }
        } else if (mS == 2) {
            if (n == 2) {
                if (this.board.getBoard()[y + 2][x] == 'd') {
                    this.board.getBoard()[y + 2][x] = 'f';
                } else {
                    this.board.getBoard()[y + 2][x] = 'b';
                }

                if (this.board.getBoard()[y + 1][x] == 'f') {
                    this.board.getBoard()[y + 1][x] = 'o';
                } else {
                    this.board.getBoard()[y + 1][x] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x, y + 1 };
                this.board.setPlayerPos(tmp);

            } else if (n == 8) {
                if (this.board.getBoard()[y - 2][x] == 'd') {
                    this.board.getBoard()[y - 2][x] = 'f';
                } else {
                    this.board.getBoard()[y - 2][x] = 'b';
                }

                if (this.board.getBoard()[y - 1][x] == 'f') {
                    this.board.getBoard()[y - 1][x] = 'o';
                } else {
                    this.board.getBoard()[y - 1][x] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x, y - 1 };
                this.board.setPlayerPos(tmp);

            } else if (n == 4) {
                if (this.board.getBoard()[y][x - 2] == 'd') {
                    this.board.getBoard()[y][x - 2] = 'f';
                } else {
                    this.board.getBoard()[y][x - 2] = 'b';
                }

                if (this.board.getBoard()[y][x - 1] == 'f') {
                    this.board.getBoard()[y][x - 1] = 'o';
                } else {
                    this.board.getBoard()[y][x - 1] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x - 1, y };
                this.board.setPlayerPos(tmp);
            }

            else if (n == 6) {
                if (this.board.getBoard()[y][x + 2] == 'd') {
                    this.board.getBoard()[y][x + 2] = 'f';
                } else {
                    this.board.getBoard()[y][x + 2] = 'b';
                }

                if (this.board.getBoard()[y][x + 1] == 'f') {
                    this.board.getBoard()[y][x + 1] = 'o';
                } else {
                    this.board.getBoard()[y][x + 1] = 'p';
                }
                if (this.board.getBoard()[y][x] == 'o') {
                    this.board.getBoard()[y][x] = 'd';
                } else {
                    this.board.getBoard()[y][x] = 'n';
                }
                int[] tmp = { x + 1, y };
                this.board.setPlayerPos(tmp);
            }
        }
        stepsCount++;
    }

    /**
     * Verifies if the game is ended : Only True if all boxes are at destination and
     * all destinations have a box ('f')
     *
     * @return boolean
     */
    public boolean verifyGameEnd() {
        for (int i = 0; i < this.board.getBoard().length; i++) {
            for (int j = 0; j < this.board.getBoard()[i].length; j++) {
                if (this.board.getBoard()[i][j] == 'b' || this.board.getBoard()[i][j] == 'o'
                        || this.board.getBoard()[i][j] == 'd') { // Actually, only box 'b' condition is sufficient in
                                                                 // our case
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Getter board
     *
     * @return Board
     */
    public Board getBoard() {
        return this.board;
    }

    /**
     * Getter steps
     *
     * @return int current steps count
     */
    public int getSteps() {
        return this.stepsCount;
    }
}
