package backend.game;

public class Launch{

    public static void main(String[] args){

        char[][] a = {{'w','w','w','w','w','w'},
                      {'w','n','n','n','n','w'},
                      {'w','p','n','b','d','w'},
                      {'w','b','n','w','n','w'},
                      {'w','d','n','n','n','w'},
                      {'w','w','w','w','w','w'}};

        Board x = new Board(a);
        if(!x.verifyLegalBoard() || !x.verifyBoardCorrectlyClosed()){
            System.out.println("illegal board");
            System.exit(0);
        }

        GameTerminal y = new GameTerminal(x);
        //System.out.println(y.getBoard().getPlayerPos()[1]);
        y.run();

        /*char[][] test1 = {{'n','n','n','n'},{'d','b','n','n'},{'b','p','n','n'},{'b','b','b','n'}};
        Board test2 = new Board(test1);
        GameTerminal test = new GameTerminal(test2);

        test.Display();
        //System.out.println(test.getBoard().getPlayerPos()[0]);
        //System.out.println(test.getBoard().getPlayerPos()[1]);
        //System.out.println(test.moveState(8)); */
    }

    /* Debug Status:
        Input Detection Debugged
        Input Processing Debugged
        Display Debugged
        Move input reception works
        MoveState input Reception works
        Move State Debugged

    */

}