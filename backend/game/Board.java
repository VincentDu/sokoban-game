package backend.game;

public class Board {

    final char[] collection;
    private char[][] board;
    private int[] playerPos;

    /* Constructor for board for variables to stock */
    public Board(char[][] board) {
        try {
            this.board = board;
        } catch (Exception e) {
            System.out.println("Bad Board input");
        }
        collection = new char[] { 'w', 'p', 'b', 'd', 'o', 'f', 'n', 'v'};

        this.playerPos = new int[] { -1, -1 };
        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[i].length; j++) {
                if (this.board[i][j] == 'p') {
                    int y = i;
                    int x = j;
                    this.playerPos = new int[] { x, y };
                }
            }
        }

        if (this.playerPos[0] == -1) {
            System.out.println("Bad Map loaded");
            System.exit(0);
        }

    }

    /*
     * The lexicon of the characters used in the board w -> Wall b -> Box p ->
     * Player d -> Destination n -> Nothing, Null, Empty (This will be rewritten if
     * we put a new item on o -> Player and Destination (Overlap) f -> Box and
     * Destination (Final), 'v' -> Void (unused items around the walls)
     */

    public int[] getPlayerPos() {
        return this.playerPos;
    }

    public void setPlayerPos(int[] x) {
        this.playerPos = x;
    }

    public char[][] getBoard() {
        return this.board;
    }

    public boolean verifyLegalCharacter(char i) {
        boolean bool = false;
        for (int j = 0; j < this.collection.length; j++) {
            if (i == this.collection[j]) { // verifying that the character is on the array of legal characters
                bool = true;
            }
        }
        return bool;
    }

    /**
     * This function will verify if a board is correctly closed, which means we will
     * have walls ['w'] around our board without holes
     * And what is ouside the closed walls must be ['v'] or ['w'] types
     * This will also verify that boxes, destinations, or the player are not stuck
     *
     * @return boolean
     */
    public boolean verifyBoardCorrectlyClosed() {
        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[i].length; j++) {
                // Check the board limits are ['v', 'w']
                if (i == 0 || j == 0 || i == this.board.length - 1 || j == this.board[i].length - 1)
                    if (this.board[i][j] != 'v' && this.board[i][j] != 'w')
                        return false;

                // Check Top/Left/Right/Down of 'v' are ['v', 'w']
                if (this.board[i][j] == 'v') {
                    // Check up
                    if (i > 0 && (this.board[i - 1][j] != 'v' && this.board[i - 1][j] != 'w'))
                        return false;
                    // Check Left
                    if (j > 0 && (this.board[i][j - 1] != 'v' && this.board[i][j - 1] != 'w'))
                        return false;
                    // Check Right
                    if (j < this.board[i].length - 1 && (this.board[i][j + 1] != 'v' && this.board[i][j + 1] != 'w'))
                        return false;
                    // Check Down
                    if (i < this.board.length - 1 && (this.board[i + 1][j] != 'v' && this.board[i + 1][j] != 'w'))
                        return false;
                }

                // Check ['b', 'd', 'o', 'f' 'p'] are not stuck
                if (this.board[i][j] != 'w' && this.board[i][j] != 'v' && this.board[i][j] != 'n') {
                    int stuck = 0;

                    // Check limits
                    if (i == 0 || i == this.board.length)
                        stuck++;
                    if (j == 0 || j == this.board[i].length)
                        stuck++;
                    // Check up
                    if (i > 0 && (this.board[i - 1][j] == 'v' || this.board[i - 1][j] == 'w'))
                        stuck++;
                    // Check Left
                    if (j > 0 && (this.board[i][j - 1] == 'v' || this.board[i][j - 1] == 'w'))
                        stuck++;
                    // Check Right
                    if (j < this.board[i].length - 1 && (this.board[i][j + 1] == 'v' || this.board[i][j + 1] == 'w'))
                        stuck++;
                    // Check Down
                    if (i < this.board.length - 1 && (this.board[i + 1][j] == 'v' || this.board[i + 1][j] == 'w'))
                        stuck++;

                    if (stuck >= 4)
                        return false;
                }
            }
        }

        return true;
    }

    public boolean verifyLegalBoard() {
        int[] check = { 0, 0, 0 }; // Temporary Array to do a multitude of tests on to verify that the game is
                                   // legal index 0 is for player, 1 for box and 2 for objective
        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[i].length; j++) {
                if (!verifyLegalCharacter(this.board[i][j])) { // verifying that no illegal character exists
                    return false;
                }
                switch (this.board[i][j]) {
                    case 'p':
                        check[0] += 1;
                        continue;
                    case 'b':
                        check[1] += 1;
                        continue;
                    case 'd':
                        check[2] += 1;
                        continue;
                    case 'o':
                        check[0] += 1;
                        check[2] += 1;
                        continue;
                }
            }
        }

        // the sets of tests that will be done using the array check
        if (check[0] != 1 || check[1] == 0 || check[2] == 0 || check[1] != check[2]) {
            return false;
        }
        return true;
    }

}
