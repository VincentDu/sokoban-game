package backend.game;

import backend.maps.Map;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;


/**
 * Class Level
 * This Class will store the Sokoban Levels in a Hashmap.
 * A Level contains a String name; and a Board board
 */
public class Level {
    private static HashMap<String, Level> levelsMap; // All maps stored Key = name
    private static int nbLevels; // This amount increases when a Level is successfully added
    private String name; // Level's unique name
    private Board board; // Board
    private boolean cleared;
    private int record;

    /**
     * Constructor of Level
     * Levels are loaded according to the json file
     * MapsStored.json
     * @param String name
     * @param Board board
     * @param boolean cleared
     */
    public Level(String name, Board board, boolean cleared, int record) {
        this.name = name;
        this.board = board;
        this.cleared = cleared;
        this.record = record;
    }

    /**
     * Constructor of Level
     * Levels are loaded according to the json file
     * MapsStored.json
     * This version takes a 2D char array
     * @param String name
     * @param char[][] board
     * @param boolean cleared
     */
    public Level(String name, char[][] board, boolean cleared, int record) {
        this.name = name;
        this.board = new Board(board); // There should not be a System.exit(0), as the presence
                                    // of the player is already verified beforehand
        this.cleared = cleared;
        this.record = record;
    }

    /**
     * Getter : Level's unique name
     * @return String name of Level
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter : Level's Board
     * @return Board
     */
    public Board getBoard() {
        return this.board;
    }

    /**
     * Getter : Level's Clear state
     * @return boolean
     */
    public boolean getCleared() {
        return this.cleared;
    }

    public int getRecord() {
        return this.record;
    }

    /**
     * Getter : All Levels Stored
     * @return HashMap<String, Level>
     */
    public static HashMap<String, Level> getSokobanLevels() {
        return levelsMap;
    }

    /**
     * Getter : Number of Levels stored
     * @return int
     */
    public static int getNbLevels() {
        return nbLevels;
    }

    /**
     * Checks if the given name is taken in the Hashmap
     * True = Is already taken
     * Can return False if the Hashmap wasn't initialized
     * @param name
     * @return boolean
     */
    public static boolean isNameTaken(String name) {
        if (levelsMap == null)
            return false;
        return levelsMap.get(name) != null;
    }

    /**
     * This will add the (pre-written) fixed maps
     * in MapsStored.json
     * Warning : This will OVERWRITE the previous content
     * in the json file
     * @throws Exception
     */
    public static void initializeLevels() throws Exception {
        try {
            levelsMap = new HashMap<>();
            nbLevels = 0;
            Map.addFixedMaps();
            Object[][] fixedMaps = Map.getFixedMaps();
            String seed; // The map as a seed
            char[][] board; // The map converted
            boolean cleared;
            int record;
            Level lvl;

            for (int i = 0; i < fixedMaps.length; i++) {
                int cpt = 0;
                seed = (String)fixedMaps[i][1];
                board = new char[(int)fixedMaps[i][2]][(int)fixedMaps[i][3]];

                for (int j = 0; j < board.length; j++) { // Fill the 2D Array
                    for (int j2 = 0; j2 < board[j].length; j2++)
                        board[j][j2] = seed.charAt(cpt++);
                }

                cleared = (int)fixedMaps[i][4] == 1;
                record = (int)fixedMaps[i][5];
                lvl = new Level((String)fixedMaps[i][0], board, cleared, record);
                levelsMap.put((String)fixedMaps[i][0], lvl);
                nbLevels++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the maps stored in MapsStored.json
     * into a Hashmap<String, Level>
     * This should be called first, but storing a level
     * will also call this function.
     * @throws Exception
     */
    public static void loadLevels() throws Exception {
        try {
            levelsMap = new HashMap<>();
            nbLevels = 0;
            Object[][] fixedMaps = Map.getFixedMaps();
            Object[][] originalMaps = Map.getOriginalMaps();
            String seed; // The map as a seed
            char[][] board; // The map converted
            boolean cleared;
            int record;
            Level lvl;
            int cpt = 0;

            // Add the Fixed Maps
            for (int i = 0; i < fixedMaps.length; i++) {
                cpt = 0;
                seed = (String)fixedMaps[i][1];
                board = new char[(int)fixedMaps[i][2]][(int)fixedMaps[i][3]];

                for (int j = 0; j < board.length; j++) { // Fill the 2D Array
                    for (int j2 = 0; j2 < board[j].length; j2++)
                        board[j][j2] = seed.charAt(cpt++);
                }

                cleared = (int)fixedMaps[i][4] == 1;
                record = (int)fixedMaps[i][5];
                lvl = new Level((String)fixedMaps[i][0], board, cleared, record);
                levelsMap.put((String)fixedMaps[i][0], lvl);
                nbLevels++;
            }

            // Add the Original Maps
            for (int i = 0; i < originalMaps.length; i++) {
                cpt = 0;
                seed = (String)originalMaps[i][1];
                board = new char[(int)originalMaps[i][2]][(int)originalMaps[i][3]];

                for (int j = 0; j < board.length; j++) { // Fill the 2D Array
                    for (int j2 = 0; j2 < board[j].length; j2++)
                        board[j][j2] = seed.charAt(cpt++);
                }

                cleared = (int)originalMaps[i][4] == 1;
                record = (int)originalMaps[i][5];
                lvl = new Level((String)originalMaps[i][0], board, cleared, record);
                levelsMap.put((String)originalMaps[i][0], lvl);
                nbLevels++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stores a new level in the Hashmap,
     * and also in MapsStored.json
     * It might fail as every levels must have
     * an unique name, but also the board
     * must be valid as a Sokoban board.
     * @param Level
     * @return boolean
     * @throws Exception
     */
    public static boolean storeLevel(Level level) throws Exception {
        try {
            if (isNameTaken(level.getName()) || !level.getBoard().verifyLegalBoard()
                || !level.getBoard().verifyBoardCorrectlyClosed())
                return false;

            nbLevels++;
            levelsMap.put(level.getName(), level);
            return Map.addNewMap(level.getName(), level.getBoard().getBoard());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Stores a new level in the Hashmap,
     * and also in MapsStored.json
     * It might fail as every levels must have
     * an unique name, but also the board
     * must be valid as a Sokoban board.
     * @param name String
     * @param board char[][]
     * @return boolean
     * @throws Exception
     */
    public static boolean storeLevel(String name, char[][] board) throws Exception {
        try {
            Board b = new Board(board);
            if (isNameTaken(name) || !b.verifyLegalBoard() || !b.verifyBoardCorrectlyClosed())
                return false;

            nbLevels++;
            Level lvl = new Level(name, board, false, 0);
            levelsMap.put(name, lvl);
            return Map.addNewMap(name, board);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Removes a level from the Hashmap,
     * and also in MapsStored.json
     * It might fail as the name might not appear.
     * @param name
     * @return boolean
     * @throws Exception
     */
    public static boolean removeLevel(String name) throws Exception {
        if (!isNameTaken(name))
            return false;
        try {
            Map.removeMap(name);
            levelsMap.remove(name);
            nbLevels--;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Sets a level to the cleared state given in parameter
     * @param name String
     * @param cleared boolean
     * @param record int
     * @throws Exception
     */
    public static void setLevelCleared(String name, boolean cleared, int score) throws Exception {
        try {
            if (isNameTaken(name)) {
                // Check if it's a new record
                if (levelsMap.get(name).record == 0 || levelsMap.get(name).record > score)
                    levelsMap.get(name).record = score;
                levelsMap.get(name).cleared = cleared;
                Map.setMapCleared(name, cleared, levelsMap.get(name).record);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the Levels from the current Hashmap,
     * and then add them in an ArrayList<Level>
     * and sorts them (by name ascending), to return.
     * This will help the Level Selector display.
     * @return ArrayList<Level> All levels sorted
     */
    public static ArrayList<Level> getSortedLevels() {
        ArrayList<Level> levelsList = new ArrayList<>(levelsMap.values()); // All levels inside

        // We sort them by their Name
        levelsList.sort(new Comparator<Level>() {
            @Override
            public int compare(Level lvl1, Level lvl2) {
                return lvl1.getName().compareTo(lvl2.getName());
            }
        });

        return levelsList;
    }

    /**
     * Gets the Fixed Levels stored,
     * Sorts them and returns them.
     * This will help the Level Selector display.
     * @return ArrayList<Level> Fixed Levels only
     * @throws Exception
     */
    public static ArrayList<Level> getSortedFixedLevels() throws Exception {
        try {
            Object[][] fixedMaps = Map.getFixedMaps();
            ArrayList<Level> fixLvList = new ArrayList<>();

            String seed; // The map as a seed
            char[][] board; // The map converted
            boolean cleared;
            int record;
            Level lvl;
            int cpt = 0;

            // Add the Fixed Maps
            for (int i = 0; i < fixedMaps.length; i++) {
                cpt = 0;
                seed = (String)fixedMaps[i][1];
                board = new char[(int)fixedMaps[i][2]][(int)fixedMaps[i][3]];

                for (int j = 0; j < board.length; j++) { // Fill the 2D Array
                    for (int j2 = 0; j2 < board[j].length; j2++)
                        board[j][j2] = seed.charAt(cpt++);
                }

                cleared = (int)fixedMaps[i][4] == 1;
                record = (int)fixedMaps[i][5];
                lvl = new Level((String)fixedMaps[i][0], board, cleared, record);
                fixLvList.add(lvl);
            }

            // Sorting List
            fixLvList.sort(new Comparator<Level>() {
                @Override
                public int compare(Level lvl1, Level lvl2) {
                    return lvl1.getName().compareTo(lvl2.getName());
                }
            });

            return fixLvList;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Gets the Original Levels stored,
     * Sorts them and returns them.
     * This will help the Level Selector display.
     * @return ArrayList<Level> Original Levels only
     * @throws Exception
     */
    public static ArrayList<Level> getSortedOriginalLevels() throws Exception {
        try {
            Object[][] originalMaps = Map.getOriginalMaps();
            ArrayList<Level> orgLvList = new ArrayList<>();

            String seed; // The map as a seed
            char[][] board; // The map converted
            boolean cleared;
            int record;
            Level lvl;
            int cpt = 0;

            // Add the Original Maps
            for (int i = 0; i < originalMaps.length; i++) {
                cpt = 0;
                seed = (String)originalMaps[i][1];
                board = new char[(int)originalMaps[i][2]][(int)originalMaps[i][3]];

                for (int j = 0; j < board.length; j++) { // Fill the 2D Array
                    for (int j2 = 0; j2 < board[j].length; j2++)
                        board[j][j2] = seed.charAt(cpt++);
                }

                cleared = (int)originalMaps[i][4] == 1;
                record = (int)originalMaps[i][5];
                lvl = new Level((String)originalMaps[i][0], board, cleared, record);
                orgLvList.add(lvl);
            }

            // Sorting List
            orgLvList.sort(new Comparator<Level>() {
                @Override
                public int compare(Level lvl1, Level lvl2) {
                    return lvl1.getName().compareTo(lvl2.getName());
                }
            });

            return orgLvList;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}