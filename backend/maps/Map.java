package backend.maps;

import org.json.*;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The class Map :
 *
 * It contains the maps of the fixed levels
 * (basic maps), we have to manually complete
 * the seeds and sizes with the maps we want in :
 * @see fixedMapsSeeds()
 * @see fixedMapsSizes()
 *
 * This class contains the methods to add, delete
 * and get the maps in our json file :
 * MapsStored.json
 */
public class Map {
    private static FileWriter file;

    // // MAIN TO RESET MAPS (exec ./reset.sh)
    public static void main(String[] args) throws Exception {
        try {
            addFixedMaps();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates the fixed map seeds
     * We add new 'basic level' maps here
     * if necessary
     * @return String[]
     */
    public static String[] fixedMapsSeeds() {
        // To complete with actual map seeds
        String [] mapSeeds =
            {
                "vvvwwww" +
                "wwwwnnw" +
                "wnnnpnw" +
                "wnbwnnw" +
                "wnnwnww" +
                "wwnwnnw" +
                "vwndnnw" +
                "vwwwwww",

                "wwwwww" +
                "wwnndw" +
                "wwnwdw" +
                "wwbbnw" +
                "wnnnnw" +
                "wpnnww" +
                "wwwwww" ,

                "vvvwwwvvvvv" +
                "vvwwnwnwwww" +
                "vwwnnwwwnnw" +
                "wwnbnnnnnnw" +
                "wnnnpbnwnnw" +
                "wwwnbwwwnnw" +
                "vvwnnwddnnw" +
                "vwwnwwdwnww" +
                "vwnnnnnnwwv" +
                "vwnnnnnwwvv" +
                "vwwwwwwwvvv" ,

                "wwwwwvv" +
                "wnnnwww" +
                "wnbnnnw" +
                "wwbwwnw" +
                "wdnnnnw" +
                "wdnpwww" +
                "wwwwwvv" ,

                "wwwwwvv" +
                "wnnnwww" +
                "wnwddnw" +
                "wnnnnnw" +
                "wwbwbww" +
                "vwnpnwv" +
                "vwwwwwv" ,

                "vwwwwvvv" +
                "vwpnwvvv" +
                "wwnbwvvv" +
                "wnndwwww" +
                "wnbddnnw" +
                "wwwwbnnw" +
                "vvvwnnww" +
                "vvvwnnwv" +
                "vvvwwwwv" ,

                "vwwvwwwww" +
                "wwnwwndnw" +
                "wnwwnbdnw" +
                "vwwnbnnnw" +
                "wwnbpnwww" +
                "wnbnnwwvv" +
                "wddnwwnww" +
                "wnnnwvwwv" +
                "wwwwwvwvv" ,

                "vvwwwww" +
                "vvwnnnw" +
                "wwwnwnw" +
                "wnbbfnw" +
                "wnwndnw" +
                "wpnndww" +
                "wwwnnwv" +
                "vvwwwwv" ,

                "wwwwwwww" +
                "wnnnnnnw" +
                "wnwbwwnw" +
                "wnwndnnw" +
                "wnbpdwnw" +
                "wnwndwnw" +
                "wnwbwwnw" +
                "wnnnnnnw" +
                "wwwwwwww" ,

                "vvvvwwwwwvvvvvvvvvvvvv" +
                "vvvvwnnnwvvvvvvvvvvvvv" +
                "vvvvwbnnwvvvvvvvvvvvvv" +
                "vvwwwnnbwwwvvvvvvvvvvv" +
                "vvwnnbnnbnwvvvvvvvvvvv" +
                "wwwnwnwwwnwvvvvvwwwwww" +
                "wnnnwnwwwnwwwwwwwnnddw" +
                "wnbnnbnnnnnnnnnnnnnddw" +
                "wwwwwnwwwwnwpwwwwnnddw" +
                "vvvvwnnnnnnwwwvvwwwwww" +
                "vvvvwwwwwwwwvvvvvvvvvv"

            };

        return mapSeeds;
    }

    /**
     * Gives the Height and Width according for each
     * map declared in fixedMapsSeeds()
     * We have an array of ints, containing the size
     * for every maps.
     * The Array of Array (should be of length 2)
     * will containt the Height and Width of the map
     * @return int[][]
     */
    public static int[][] fixedMapsSizes() {
        int [][] mapSize = // {Height, Width}
            {
                {8, 7},
                {7, 6},
                {11, 11},
                {7, 7},
                {7, 7},
                {9, 8},
                {9, 9},
                {8, 7},
                {9, 8},
                {11, 22}
            };
        return mapSize;
    }

    /**
     * This adds some fixed maps in the json file as
     * a JSONArray, and an empty JSONArray for
     * original map (created by the user(s))
     * it is called first as it will erase the previous data
     * Added maps will be stored in MapsStored.json
     * @throws Exception
     */
    public static void addFixedMaps() throws Exception {
        String [] mapSeeds = fixedMapsSeeds();
        int [][] mapSizes = fixedMapsSizes();

        JSONArray fixedMaps = new JSONArray();
        // originalMaps is empty
        JSONArray originalMaps = new JSONArray();

        // Adding the basic maps
        for (int i = 0; i < mapSeeds.length; i++) {
            JSONObject obj = new JSONObject();
            // Let's suppose we won't have that many fixed maps
            // (98 max for appropriate future display)
            if (i + 1 < 10) // 01, 02... 09
                obj.put("name", "Fixed Map 0" + (i + 1));
            else // 10, 11, ... 99
                obj.put("name", "Fixed Map " + (i + 1));
            obj.put("seed", mapSeeds[i]);
            obj.put("height", mapSizes[i][0]);
            obj.put("width", mapSizes[i][1]);
            obj.put("cleared", 0);
            obj.put("record", 0);
            fixedMaps.put(obj);
        }

        // The JSON Object we add in MapsStored.json
        JSONObject mainObj = new JSONObject();
        mainObj.put("Fixed Maps", fixedMaps);
        mainObj.put("Original Maps", originalMaps);

        try {
            file = new FileWriter("backend/maps/MapsStored.json");
            file.write(mainObj.toString(4));

            // Close the file opened
            file.flush();
            file.close();

        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    /**
     * Gets the fixed maps from the json file
     * We have the data as an array of array of Object
     * with Obj[Map_Number][0-5] (to cast into String/int)
     * (in order, 0-5 : name(str), seed(str), height(int), width(int), cleared(bool), record(int))
     * @return Object[][]
     * @throws Exception
     */
    public static Object[][] getFixedMaps() throws Exception {
        try {
            // Getting all data in a String
            String text = new String(Files.readAllBytes(
                Paths.get("backend/maps/MapsStored.json")), StandardCharsets.UTF_8);

            // Converting data into JSON Objects to manipulate
            JSONObject obj = new JSONObject(text);
            JSONArray fixedMaps = obj.getJSONArray("Fixed Maps");

            Object [][] mapsSeed = new Object [fixedMaps.length()][6];
            for (int i = 0; i < mapsSeed.length; i++) {
                mapsSeed[i][0] = fixedMaps.getJSONObject(i).getString("name");
                mapsSeed[i][1] = fixedMaps.getJSONObject(i).getString("seed");
                mapsSeed[i][2] = fixedMaps.getJSONObject(i).getInt("height");
                mapsSeed[i][3] = fixedMaps.getJSONObject(i).getInt("width");
                mapsSeed[i][4] = fixedMaps.getJSONObject(i).getInt("cleared");
                mapsSeed[i][5] = fixedMaps.getJSONObject(i).getInt("record");
            }
            return mapsSeed;

        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return null;
    }

    /**
     * Gets the original maps from the json file
     * We have the data as an array of array of Object
     * with Obj[Map_Number][0-5] (to cast into String/int)
     * (in order, 0-5 : name(str), seed(str), height(int), width(int), cleared(bool), record(int))
     * @return Object[][]
     * @throws Exception
     */
    public static Object[][] getOriginalMaps() throws Exception {
        try {
            // Getting all data in a String
            String text = new String(Files.readAllBytes(
                Paths.get("backend/maps/MapsStored.json")), StandardCharsets.UTF_8);

            // Converting data into JSON Objects to manipulate
            JSONObject obj = new JSONObject(text);
            JSONArray originalMaps = obj.getJSONArray("Original Maps");

            Object [][] mapsSeed = new Object [originalMaps.length()][6];
            for (int i = 0; i < mapsSeed.length; i++) {
                mapsSeed[i][0] = originalMaps.getJSONObject(i).getString("name");
                mapsSeed[i][1] = originalMaps.getJSONObject(i).getString("seed");
                mapsSeed[i][2] = originalMaps.getJSONObject(i).getInt("height");
                mapsSeed[i][3] = originalMaps.getJSONObject(i).getInt("width");
                mapsSeed[i][4] = originalMaps.getJSONObject(i).getInt("cleared");
                mapsSeed[i][5] = originalMaps.getJSONObject(i).getInt("record");
            }
            return mapsSeed;

        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return null;
    }

    /**
     * Adds a new map into the MapsStored.json
     * file, as an Original Map
     * A new map can be added only if its name
     * isn't already registered
     *
     * @param name String
     * @param seed String
     * @param height int
     * @param width int
     * @return boolean
     */
    public static boolean addNewMap(String name, String seed, int height, int width) throws Exception {
        try {
            if (name.equals("") || seed.equals("") || height <= 0 || width <= 0)
                return false;

            // Getting all data in a String
            String text = new String(Files.readAllBytes(
                Paths.get("backend/maps/MapsStored.json")), StandardCharsets.UTF_8);

            // Converting data into JSON Objects to manipulate
            JSONObject obj = new JSONObject(text);
            JSONArray fixedMaps = obj.getJSONArray("Fixed Maps");
            JSONArray originalMaps = obj.getJSONArray("Original Maps");

            // checking if there's an existing map with the same name
            for (int i = 0; i < fixedMaps.length(); i++) {
                if (fixedMaps.getJSONObject(i).getString("name").equals(name))
                    return false;
            }
            for (int i = 0; i < originalMaps.length(); i++) {
                if (originalMaps.getJSONObject(i).getString("name").equals(name))
                    return false;
            }

            // Preparing the new map, and the old maps
            JSONObject newMap = new JSONObject();
            newMap.put("name", name);
            newMap.put("seed", seed);
            newMap.put("height", height);
            newMap.put("width", width);
            newMap.put("cleared", 0);
            newMap.put("record", 0);
            originalMaps.put(newMap);

            JSONObject mainObj = new JSONObject();
            mainObj.put("Fixed Maps", fixedMaps);
            mainObj.put("Original Maps", originalMaps);

            // Adding the maps (overwrites the file)
            file = new FileWriter("backend/maps/MapsStored.json");
            file.write(mainObj.toString(4));

            // Close the file opened
            file.flush();
            file.close();

            return true;

        } catch (Exception ie) {
            ie.printStackTrace();
        }
        return false;
    }

    /**
     * Adds a new map into the MapsStored.json
     * file, as an Original Map
     * A new map can be added only if its name
     * isn't already registered
     *
     * @param name String
     * @param board char[][]
     * @return boolean
     */
    public static boolean addNewMap(String name, char[][] board) throws Exception {
        try {
            // Transform char[][] into String
            String seed = "";
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    seed += board[i][j];
                }
            }
            if (name.equals("") || seed.equals("") || board.length <= 0 || board[0].length <= 0)
                return false;

            return addNewMap(name, seed, board.length, board[0].length);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return false;
    }

    /**
     * Removes the Original Map with the given name
     * @param name String
     */
    public static void removeMap(String name) throws Exception {
        try {
            // Getting all data in a String
            String text = new String(Files.readAllBytes(
                Paths.get("backend/maps/MapsStored.json")), StandardCharsets.UTF_8);

            // Converting data into JSON Objects to manipulate
            JSONObject obj = new JSONObject(text);
            JSONArray originalMaps = obj.getJSONArray("Original Maps");

            // checking if there's an existing map with the same name
            for (int i = 0; i < originalMaps.length(); i++) {
                if (originalMaps.getJSONObject(i).getString("name").equals(name))
                    originalMaps.remove(i);
            }

            // Preparing the maps to add
            JSONArray fixedMaps = obj.getJSONArray("Fixed Maps");
            JSONObject mainObj = new JSONObject();
            mainObj.put("Fixed Maps", fixedMaps);
            mainObj.put("Original Maps", originalMaps);

            // Adding the maps (overwrites the file)
            file = new FileWriter("backend/maps/MapsStored.json");
            file.write(mainObj.toString(4));

            // Close the file opened
            file.flush();
            file.close();

        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    /**
     * Sets the map with the given name to
     * Cleared (if true) or uncleared (if false)
     * Also sets the new record accordingly
     * The maps checked are in both Fixed and Original maps
     * The order is messed up (last modified is last place),
     * so we need to carefully display lexicographically
     * @param name (String)
     * @param cleared (boolean)
     * @param record (int)
     * @throws Exception
     */
    public static void setMapCleared(String name, boolean cleared, int record) throws Exception {
        try {
            // Getting all data in a String
            String text = new String(Files.readAllBytes(
                Paths.get("backend/maps/MapsStored.json")), StandardCharsets.UTF_8);

            // Converting data into JSON Objects to manipulate
            JSONObject obj = new JSONObject(text);
            JSONArray fixedMaps = obj.getJSONArray("Fixed Maps");
            JSONArray originalMaps = obj.getJSONArray("Original Maps");
            JSONObject newMap = new JSONObject();
            int clear = cleared ? 1 : 0;

            // checking if there's an existing map with the same name
            for (int i = 0; i < fixedMaps.length(); i++) {
                if (fixedMaps.getJSONObject(i).getString("name").equals(name)) {
                    newMap.put("name", fixedMaps.getJSONObject(i).getString("name"));
                    newMap.put("seed", fixedMaps.getJSONObject(i).getString("seed"));
                    newMap.put("height", fixedMaps.getJSONObject(i).getInt("height"));
                    newMap.put("width", fixedMaps.getJSONObject(i).getInt("width"));
                    newMap.put("cleared", clear);
                    newMap.put("record", record);
                    fixedMaps.remove(i);
                    fixedMaps.put(newMap);
                    break;
                }
            }
            for (int i = 0; i < originalMaps.length(); i++) {
                if (originalMaps.getJSONObject(i).getString("name").equals(name)) {
                    newMap.put("name", originalMaps.getJSONObject(i).getString("name"));
                    newMap.put("seed", originalMaps.getJSONObject(i).getString("seed"));
                    newMap.put("height", originalMaps.getJSONObject(i).getInt("height"));
                    newMap.put("width", originalMaps.getJSONObject(i).getInt("width"));
                    newMap.put("cleared", clear);
                    newMap.put("record", record);
                    originalMaps.remove(i);
                    originalMaps.put(newMap);
                    break;
                }
            }

            // Preparing the new map, and the old maps
            JSONObject mainObj = new JSONObject();
            mainObj.put("Fixed Maps", fixedMaps);
            mainObj.put("Original Maps", originalMaps);

            // Adding the maps (overwrites the file)
            file = new FileWriter("backend/maps/MapsStored.json");
            file.write(mainObj.toString(4));

            // Close the file opened
            file.flush();
            file.close();

        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    /**
     * Same as setMapCleared(String, boolean),
     * but this function will Clear of Unclear all maps
     * @see setMapCleared(String name, boolean cleared)
     * @param cleared
     * @throws Exception
     */
    public static void setAllMapsCleared(boolean cleared) throws Exception {
        try {
            // Getting all data in a String
            String text = new String(Files.readAllBytes(
                Paths.get("backend/maps/MapsStored.json")), StandardCharsets.UTF_8);

            // Converting data into JSON Objects to manipulate
            JSONObject obj = new JSONObject(text);
            JSONArray fixedMaps = obj.getJSONArray("Fixed Maps");
            JSONArray originalMaps = obj.getJSONArray("Original Maps");

            // Preparing new maps to add
            JSONArray newFixedMaps = new JSONArray();
            JSONArray newOriginalMaps = new JSONArray();
            JSONObject newMap = new JSONObject();
            int clear = cleared ? 1 : 0;

            // Modifies all the maps by removing, changing then adding again
            for (int i = 0; i < fixedMaps.length(); i++) {
                    newMap.put("name", fixedMaps.getJSONObject(i).getString("name"));
                    newMap.put("seed", fixedMaps.getJSONObject(i).getString("seed"));
                    newMap.put("height", fixedMaps.getJSONObject(i).getInt("height"));
                    newMap.put("width", fixedMaps.getJSONObject(i).getInt("width"));
                    newMap.put("cleared", clear);
                    newMap.put("record", fixedMaps.getJSONObject(i).getInt("record"));
                    fixedMaps.remove(i);
                    i--;
                    newFixedMaps.put(newMap);
                    newMap = new JSONObject();
            }
            for (int i = 0; i < originalMaps.length(); i++) {
                    newMap.put("name", originalMaps.getJSONObject(i).getString("name"));
                    newMap.put("seed", originalMaps.getJSONObject(i).getString("seed"));
                    newMap.put("height", originalMaps.getJSONObject(i).getInt("height"));
                    newMap.put("width", originalMaps.getJSONObject(i).getInt("width"));
                    newMap.put("cleared", clear);
                    newMap.put("record", originalMaps.getJSONObject(i).getInt("record"));
                    originalMaps.remove(i);
                    i--;
                    newOriginalMaps.put(newMap);
                    newMap = new JSONObject();
            }

            // Adding the new maps
            JSONObject mainObj = new JSONObject();
            mainObj.put("Fixed Maps", newFixedMaps);
            mainObj.put("Original Maps", newOriginalMaps);

            // Adding the maps (overwrites the file)
            file = new FileWriter("backend/maps/MapsStored.json");
            file.write(mainObj.toString(4));

            // Close the file opened
            file.flush();
            file.close();

        } catch (IOException ie) {
            ie.printStackTrace();
        }

    }
}