package test;

import java.util.Arrays;

import backend.game.Board;
import backend.game.GameTerminal;

/**
 * This class is a small mimic to
 * JUnit Tests, it will be used
 * to test the methods from the backend
 */
public class TestRunner {
    private static int numberOfTests = 0;
    private static int numberPassed = 0;
    private static int numberFailed = 0;

    /**
     * Prints the result of a test with its message
     * @param actual what we get from calling a method
     * @param expected what we should have
     * @param message the kind of test we are doing
     */
    public static void myAssertEquals(Object actual, Object expected, String message) {
        numberOfTests++;
        System.out.print("Test Number " + numberOfTests + " : " + message + " has ");
        if (actual != null && expected != null) {
            if (actual.equals(expected)) {
                System.out.println("PASSED.");
                numberPassed++;
            } else {
                System.out.println("FAILED. * * * * *");
                numberFailed++;
            }
        } else {
            System.out.println("FAILED. * * * * *");
            numberFailed++;
        }
    }

    /**
     * Prints the result of a test with its message
     * @param myAssert a boolean
     * @param message the kind of test we are doing
     */
    public static void myAssertTrue(boolean myAssert, String message) {
        numberOfTests++;
        System.out.print("Test Number " + numberOfTests + " : " + message + " has ");
        if (myAssert) {
            System.out.println("PASSED.");
            numberPassed++;
        } else {
            System.out.println("FAILED. * * * * *");
            numberFailed++;
        }
    }

    /**
     * This displays the final results of the tests
     * With the detailed number of tests passed / failed
     */
    public static void myTestResults() {
        System.out.println("\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n");
        System.out.println("Results of the tests :");
        System.out.println("-Number of Tests = " + numberOfTests);
        System.out.println("-Tests PASSED = " + numberPassed);
        System.out.println("-Tests FAILED = " + numberFailed);
        System.out.println("\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n");
    }

    /**
     * WRITE THE TESTS HERE
     * @param args
     */
    public static void main(String[] args) {

        char[][] c = {
            {'w','w','w','w','w','w'},
            {'w','n','n','n','n','w'},
            {'w','p','n','b','d','w'},
            {'w','b','n','w','n','w'},
            {'w','d','n','n','n','w'},
            {'w','w','w','w','w','w'}
        };
        Board b = new Board(c);


        // * * * * * * * * * * Tests for verifyLegalCharacter in Board.java * * * * * * * * * *
        myAssertTrue(b.verifyLegalCharacter(b.getBoard()[0][0]), "Board char wall");
        myAssertTrue(b.verifyLegalCharacter(b.getBoard()[1][1]), "Board char player");
        myAssertTrue(b.verifyLegalCharacter(b.getBoard()[3][1]), "Board char null");
        myAssertTrue(!b.verifyLegalCharacter('a'), "Char a");
        myAssertTrue(!b.verifyLegalCharacter('#'), "Char #");
        // * * * * * * * * * * End of tests for verifyLegalCharacter in Board.java * * * * * * * * * *


        // * * * * * * * * * * Tests for verifyLegalBoard in Board.java * * * * * * * * * *
        char[][] more_boxes = {
            {'w', 'w', 'p', 'n', 'w'},
            {'w', 'b', 'n', 'b', 'w'},
            {'w', 'd', 'b', 'd', 'w'},
            {'w', 'w', 'n', 'w', 'w'}
        };
        Board b_more_boxes = new Board(more_boxes);

        char[][] more_dest = {
            {'w', 'w', 'p', 'n', 'w'},
            {'w', 'b', 'n', 'b', 'w'},
            {'w', 'd', 'd', 'd', 'w'},
            {'w', 'w', 'n', 'w', 'w'}
        };
        Board b_more_dest = new Board(more_dest);

        myAssertTrue(b.verifyLegalBoard(), "Legal Board");
        myAssertTrue(!b_more_boxes.verifyLegalBoard(), "More boxes than destinations");
        myAssertTrue(!b_more_dest.verifyLegalBoard(), "More destinations than boxes");

        more_dest[2][2] = 'o';
        Board b_more_dest_o = new Board(more_dest);
        myAssertTrue(!b_more_dest_o.verifyLegalBoard(), "More destinations than boxes (due to overlap)");

        char[][] map_players_nb = { // 0 players
            {'w', 'w', 'n', 'n', 'w'},
            {'w', 'b', 'n', 'b', 'w'},
            {'w', 'd', 'n', 'd', 'w'},
            {'w', 'w', 'n', 'w', 'w'}
        };

        // These following 2 lines exits with "Bad Board Imput" print
        // Board b_miss_player = new Board(map_players_nb);
        // myAssertTrue(!b_miss_player.verifyLegalBoard(), "Too many players");

        map_players_nb[0][2] = 'p';
        map_players_nb[2][2] = 'p'; // 2 players
        Board b_two_players = new Board(map_players_nb);
        myAssertTrue(!b_two_players.verifyLegalBoard(), "Not enough players");

        char[][] no_destination_nor_box = {
            {'w', 'w', 'w'},
            {'w', 'p', 'w'},
            {'w', 'w', 'w'}
        };
        Board b_no_d_nor_b = new Board(no_destination_nor_box);
        myAssertTrue(!b_no_d_nor_b.verifyLegalBoard(), "0 destination and 0 box");
        // * * * * * * * * * * End of tests for verifyLegalBoard in Board.java * * * * * * * * * *


        char [][] board = {
            {'w','w','w','w','w','w'},
            {'w','n','n','n','n','w'},
            {'w','p','n','b','d','w'},
            {'w','b','n','w','n','w'},
            {'w','d','n','n','n','w'},
            {'w','w','w','w','w','w'}
        };
        Board gameBoard = new Board(board);
        GameTerminal gt1 = new GameTerminal(gameBoard);
        char [][] board2 = {
            {'w','w','w','w','w','w'},
            {'w','n','n','n','n','w'},
            {'w','p','n','b','d','w'},
            {'w','b','n','w','n','w'},
            {'w','d','n','n','n','w'},
            {'w','w','w','w','w','w'}
        };


        // * * * * * * * * * * Tests for moveState in GameTerminal.java * * * * * * * * * *
        // Allowed moves are with parameter {2,4,6,8} corresponding to {down, left, right, up}
        myAssertEquals(gt1.moveState(2), 2, "Box movement allowed");
        myAssertEquals(gt1.moveState(4), 0, "Move in wall not allowed");
        myAssertEquals(gt1.moveState(6), 1, "Move in empty space allowed (right)");
        myAssertEquals(gt1.moveState(8), 1, "Move in empty space allowed (up)");

        myAssertEquals(gt1.moveState(0), 0, "Not recognized move (0)");
        myAssertEquals(gt1.moveState(1), 0, "Not recognized move (1)");

        board[2][2] = 'b';
        myAssertEquals(gt1.moveState(6), 0, "2 consecutive boxes, movement not allowed");
        board[2][2] = 'd';
        myAssertEquals(gt1.moveState(6), 1, "Move in destination allowed");
        board[2][2] = 'o';

        // Should never happen in classic Sokoban
        myAssertEquals(gt1.moveState(6), 0, "Move in overlap (destination + player) not allowed");
        board[2][2] = 'n'; // back to normal
        board[4][1] = 'f';
        myAssertEquals(gt1.moveState(2), 0, "Move in box & final (consecutive) not allowed");
        board[4][1] = 'w';
        myAssertEquals(gt1.moveState(2), 0, "Move in box & wall (consecutive) not allowed");
        board[3][1] = 'f';
        board[4][1] = 'd';
        myAssertEquals(gt1.moveState(2), 2, "Move in final allowed");
        board[4][1] = 'w';
        myAssertEquals(gt1.moveState(2), 0, "Move in final & wall (consecutive) not allowed");
        board[4][1] = 'b';
        myAssertEquals(gt1.moveState(2), 0, "Move in final & box (consecutive) not allowed");
        board[3][1] = 'f';
        board[4][1] = 'f';
        myAssertEquals(gt1.moveState(2), 0, "Move in final & final (consecutive) not allowed");
        board[3][1] = 'b';
        board[4][1] = 'd';
        // * * * * * * * * * * End of tests for moveState in GameTerminal.java * * * * * * * * * *


        // * * * * * * * * * * Tests for verifyLegalMove in GameTerminal.java * * * * * * * * * *
        myAssertTrue(gt1.verifyLegalMove(gt1.moveState(6)), "Legal move (to n)");
        myAssertTrue(gt1.verifyLegalMove(gt1.moveState(2)), "Legal move (to b)");
        myAssertTrue(!gt1.verifyLegalMove(gt1.moveState(4)), "Illegal move (to w)");
        myAssertTrue(!gt1.verifyLegalMove(0), "Parameter int = 0 -> Illegal");
        myAssertTrue(gt1.verifyLegalMove(1), "Parameter int = 1 -> Legal");
        myAssertTrue(gt1.verifyLegalMove(2), "Parameter int = 2 -> Legal");
        // * * * * * * * * * * End of tests for verifyLegalMove in GameTerminal.java * * * * * * * * * *


        // * * * * * * * * * * Tests for verifyGameEnd in GameTerminal.java * * * * * * * * * *
        myAssertTrue(!gt1.verifyGameEnd(), "Game started, not finished");
        board[3][1] = 'n';
        board[4][1] = 'f'; // One final, second not final
        myAssertTrue(!gt1.verifyGameEnd(), "Game not finished, not all finals");
        board[2][4] = 'n';
        board[2][1] = 'o';
        myAssertTrue(!gt1.verifyGameEnd(), "Game not finished, one overlap, others finals");
        board[2][4] = 'f';
        board[2][1] = 'p';
        board[2][3] = 'n';
        myAssertTrue(gt1.verifyGameEnd(), "Game finished classic scenario");
        board[3][1] = 'b';
        board[4][1] = 'd';
        board[2][3] = 'b';
        board[2][4] = 'd';
        myAssertTrue(Arrays.deepEquals(board, board2), "The 2 boards should be the same");
        // * * * * * * * * * * End of tests for verifyGameEnd in GameTerminal.java * * * * * * * * * *


        // * * * * * * * * * * Tests for move in GameTerminal.java * * * * * * * * * *
        gt1.move(2, gt1.moveState(2)); // Pushes box to destination
        board2[2][1] = 'n';
        board2[3][1] = 'p';
        board2[4][1] = 'f';
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement down, pushed box to destination");

        // Some not allowed movements
        gt1.move(2, gt1.moveState(2)); // Doesn't do anything
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not allowed, final to wall (down)");
        gt1.move(8, 10); // Doesn't do anything
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not granted, wrong parameter");
        gt1.move(4, gt1.moveState(4));
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not granted, not allowed (left)");

        // Set up boxes & finals to test moves
        gt1.move(8, gt1.moveState(8));
        board2[3][1] = 'n';
        board2[2][1] = 'p';
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement up");

        // Some not allowed moves
        board[3][1] = 'b'; // set-up consecutive b and f
        board2[3][1] = 'b';
        gt1.move(4, gt1.moveState(4));
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not allowed, box to final (down)");
        board[3][1] = 'f'; // set-up 2 consecutive f
        board2[3][1] = 'f';
        gt1.move(4, gt1.moveState(4));
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not allowed, final to final (down)");
        board[4][1] = 'b'; // set-up consecutive f and b
        board2[4][1] = 'b';
        gt1.move(4, gt1.moveState(4));
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not allowed, final to box (down)");
        board[3][1] = 'b'; // set-up 2 consecutive b
        board2[3][1] = 'b';
        gt1.move(4, gt1.moveState(4));
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not allowed, box to box (down)");
        board[4][1] = 'w'; // set-up consecutive b and w
        board2[4][1] = 'w';
        gt1.move(4, gt1.moveState(4));
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement not allowed, box to wall (down)");
        board[3][1] = 'f'; // set-up consecutive f and n
        board2[3][1] = 'f';
        board[4][1] = 'n';
        board2[4][1] = 'n';

        gt1.move(2, gt1.moveState(2));
        board2[2][1] = 'n'; // board2 follows
        board2[3][1] = 'o';
        board2[4][1] = 'b';
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement allowed, final (down)");
        gt1.move(8, gt1.moveState(8));
        board2[2][1] = 'p';
        board2[3][1] = 'd';
        myAssertTrue(Arrays.deepEquals(board, board2), "Movement allowed, normal (up)");
        board[3][1] = 'n'; // reset back to "normal" before set-ups tests
        board2[3][1] = 'n';
        board[4][1] = 'f';
        board2[4][1] = 'f';

        // moving to a destination (overlap)
        gt1.move(6, gt1.moveState(6)); // move right
        gt1.move(8, gt1.moveState(8)); // move up
        gt1.move(6, gt1.moveState(6)); // move right
        gt1.move(6, gt1.moveState(6)); // move right
        gt1.move(2, gt1.moveState(2)); // move down
        board2[2][1] = 'n';
        board2[2][4] = 'o';
        myAssertTrue(Arrays.deepEquals(board, board2), "Moving to a destination (overlap), not moving any blocks");

        // Going around the wall to move box to destination (last f)
        gt1.move(2, gt1.moveState(2)); // move down
        gt1.move(2, gt1.moveState(2)); // move down
        gt1.move(2, gt1.moveState(2)); // move down
        gt1.move(4, gt1.moveState(4)); // move left
        gt1.move(4, gt1.moveState(4)); // move left
        gt1.move(8, gt1.moveState(8)); // move up
        gt1.move(8, gt1.moveState(8)); // move up
        gt1.move(6, gt1.moveState(6)); // move right
        board2[2][3] = 'p';
        board2[2][4] = 'f';
        myAssertTrue(Arrays.deepEquals(board, board2), "Moving back, pushing last box to destination");
        myAssertTrue(gt1.verifyGameEnd(), "Game successfully ended with real moves");
        // * * * * * * * * * * End of tests for move in GameTerminal.java * * * * * * * * * *


        // * * * * * * * * * * Tests for verifyBoardCorrectlyClosed in GameTerminal.java * * * * * * * * * *
        char[][] correctlyClosedBoard = {
            {'v','v','v','w','w','v'},
            {'v','w','w','n','n','w'},
            {'w','p','n','b','d','w'},
            {'w','b','n','w','n','w'},
            {'w','d','n','n','n','w'},
            {'v','w','w','w','w','v'}
        };
        Board ccBoard = new Board(correctlyClosedBoard);

        char[][] uncorrectlyClosedBoard = {
            {'n','v','v','w','w','v'},
            {'v','w','w','n','n','w'},
            {'w','p','n','b','d','w'},
            {'w','b','n','w','n','w'},
            {'w','d','n','n','n','w'},
            {'v','w','w','w','w','v'}
        };
        Board ucBoard = new Board(uncorrectlyClosedBoard);

        myAssertTrue(ccBoard.verifyBoardCorrectlyClosed(), "Board Correctly Closed");
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "'n' at index [0][0]");
        uncorrectlyClosedBoard[0][0] = 'v'; // Back to correctly closed
        uncorrectlyClosedBoard[5][5] = 'b';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "'b' at index [5][5] (limit)");
        uncorrectlyClosedBoard[5][5] = 'v'; // Back to correctly closed
        uncorrectlyClosedBoard[1][1] = 'n';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "'n' at index [1][1] : walls not closed, there's a hole.");
        uncorrectlyClosedBoard[1][1] = 'w'; // Back to correctly closed

        uncorrectlyClosedBoard[2][2] = 'w';
        uncorrectlyClosedBoard[3][1] = 'w';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "Player stuck between 4 walls.");
        uncorrectlyClosedBoard[2][1] = 'b';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "Box stuck between 4 walls.");
        uncorrectlyClosedBoard[2][1] = 'd';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "Destination stuck between 4 walls.");
        uncorrectlyClosedBoard[2][1] = 'f';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "Final stuck between 4 walls.");
        uncorrectlyClosedBoard[2][1] = 'o';
        myAssertTrue(!ucBoard.verifyBoardCorrectlyClosed(), "Overlap stuck between 4 walls.");

        uncorrectlyClosedBoard[2][1] = 'p'; // Back to normal
        uncorrectlyClosedBoard[2][2] = 'n'; // Back to correctly closed
        uncorrectlyClosedBoard[3][1] = 'b'; // Back to correctly closed
        myAssertTrue(ucBoard.verifyBoardCorrectlyClosed(), "Board is back to correctly closed.");
        // * * * * * * * * * * End of tests for verifyBoardCorrectlyClosed in GameTerminal.java * * * * * * * * * *


        // * * * * * * * * * * Tests for stepsCount in GameTerminal.java * * * * * * * * * *
        char[][] boardForCountingSteps = {
            {'v','v','v','w','w','v'},
            {'v','w','w','n','n','w'},
            {'w','p','n','b','d','w'},
            {'w','b','n','w','n','w'},
            {'w','d','n','n','n','w'},
            {'v','w','w','w','w','v'}
        };
        Board stepCountBoard = new Board(boardForCountingSteps);
        GameTerminal gameSteps = new GameTerminal(stepCountBoard);

        myAssertEquals(gameSteps.getSteps(), 0, "No moves done.");

        gameSteps.move(4, gameSteps.moveState(4)); // Move left, no changes
        myAssertEquals(gameSteps.getSteps(), 0, "Move not allowed (to wall), same number of steps.");

        gameSteps.move(2, gameSteps.moveState(2)); // Move down, push box, 1 step
        myAssertEquals(gameSteps.getSteps(), 1, "1 step : Move down, push box to destination.");

        gameSteps.move(2, gameSteps.moveState(2)); // Move down, 1 step
        myAssertEquals(gameSteps.getSteps(), 1, "Move not allowed (to box), same number of steps.");

        gameSteps.move(6, gameSteps.moveState(6)); // Move right, 1 step
        gameSteps.move(2, gameSteps.moveState(2)); // Move down, 1 step
        gameSteps.move(8, gameSteps.moveState(8)); // Move up, 1 step
        gameSteps.move(8, gameSteps.moveState(8)); // Move up, 1 step
        gameSteps.move(4, gameSteps.moveState(4)); // Move left, 1 step
        gameSteps.move(6, gameSteps.moveState(6)); // Move right, 1 step
        gameSteps.move(6, gameSteps.moveState(6)); // Move right, 1 step
        myAssertEquals(gameSteps.getSteps(), 8, "7 Steps added, from 1 to 8.");

        // * * * * * * * * * * End of tests for stepsCount in GameTerminal.java * * * * * * * * * *


        // Un-Setting variables
        c = null;
        b = null;
        more_boxes = null;
        b_more_boxes = null;
        more_dest = null;
        b_more_dest = null;
        b_more_dest_o = null;
        map_players_nb = null;
        b_two_players = null;
        b_no_d_nor_b = null;
        board = null;
        gameBoard = null;
        gt1 = null;
        board2 = null;
        correctlyClosedBoard = null;
        ccBoard = null;
        uncorrectlyClosedBoard = null;
        ucBoard = null;
        boardForCountingSteps = null;
        stepCountBoard = null;
        gameSteps = null;

        myTestResults(); // Last line SUMMARY OF TESTS (Results)
    }
}